<?php

/*
Purpose: This is launched by the Ratchet websocket server located in the chat folder. It is responsible for placing the data the sensors are streaming to the server into files
         in the "dataFiles" folder. It also invokes functions that perform analysis on that data, and store the analysis results. Additionally, it stores completed data files,
         where completed means that the user is done streaming data to that file, in the "dataOutputs" folder.
*/
namespace ChatApp;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

include "uploadResultsToAnalysisTable.php"; 

//$dataFile = fopen("dataFile.txt", "w");

/************************************************
The Sensor class is used to represent the triaxal body sensors (for example, Accelerometers, Gyroscope, ECG)
that are currently communicating with the Ratchet websocket. 

********************IMP: While the class is called Sensor, it is really  treated as a "Sensor Axis" class in the code.  
***********************************************/

class Sensor{
  public $initialized;
  public $userID;
  public $sensorID;
  public $type;
  public $units;
  public $humanST;
  public $streamID;
  public $fileName;
  public $sensorFile;
  public $sensorDataEntryCount = 0; 
}


/************************************************
The Chat class extends the Ratchet MessageComponentInterface. This interface has the functions
necessary for the client (the sensor's in this use case) to communicate with the Ratchet websocket meaningfully.
These are implemented below and are onOpen, onClose, and onError. The other functions are not part of the interface. 

Variable types:
   clients: A splee for storing objects. This stores the incoming data streams' sensor information
   sensors: An associative array that stores sensor objects  
************************************************/

class Chat implements MessageComponentInterface {
  //protected $db;
  protected $clients;
  protected $count;
  protected $timing_count;
  protected $time_start;
  protected $time_end_do_analysis;
  protected $time_end;
  protected $countGoal; 

  protected $sensors; //this is an array
  protected $emailAsKeyForSensorKeyArray; 

  public function dir_is_empty($dir) {
    $h = opendir($dir);
    while (($entry = readdir($h)) !== false) {
      if($entry != "." && $entry != "..") {
        return FALSE;
      }
    }
    return TRUE;
  }

  public function most_recent_entry() {
    $query = "SELECT * FROM Datasets ORDER BY dataId DESC LIMIT 0,1";
    if($this->conn->query($query))
    {
      $res = mysqli_query($this->conn, $query);
      $r = mysqli_fetch_object($res);
      return $r;
    }
    return -1;
  }

  public function writeToCSV($sense, $data) {
    $fName = $sense->fileName;
    $sfName = $sense->sensorFile;
    //echo "masterFileName: ".$sfName."\n";
    $result = shell_exec('python ChatApp/writeCSV1.py ' . $data . ' ' . $fName);
  }

  /*
   
   When a sensor begins streaming data it will create a CSV file, aka, an entry,for each of its x,y,z axes if they are the first of their kind. 
   When the app sends the "Done" message in the control socket, it effectively closes that entry. 
   However, sometimes the connection is dropped before the app can send the "Done" message. 
   This function will look at the incoming sensor component (either its z,y, or z) to see if it has an "open" entry, and it will continue to stream data to it
   until that entry is closed. At which point, it will create a new entry for the new sensor. Every unique sensor has its own entry. So any one sensor
   can only have 
  */
  public function db_entry_check($sense, $client) {
    $t = implode('', $sense->type);
    echo "\n";
    echo "Sensor type in db_entry_check is $t \n"; 
    $query = "SELECT * FROM Datasets
                   WHERE (done = 0)
                   AND (userId = '$sense->userID')
		   AND (type = '$t')
		   AND (source = 'app')";
    $result = mysqli_query($this->conn, $query);
    $r = mysqli_fetch_array($result);
    
     
    $id = $r['streamId'];
    echo "$id\n"; 
    if(!($id == ''))
    {
      echo "found\n";
      $sense->fileName = $r['fileName'];
      //adding this line to see if it helps with the null bug. Take out if this causes more issues ***********************
      $sense->streamID = $id; 
      //*****************************************************************************************************************
      echo "$sense->fileName \n"; 
    }
    else
    {
      echo "Inside streamId incrementing section\n"; 
      $query = "SELECT * FROM Datasets
		     WHERE (done = 0)
		     AND (userId = '$sense->userID')";
      $result = mysqli_query($this->conn, $query);
      $r = mysqli_fetch_array($result);
      $sID = $r['streamId'];
      if($sID == '') {
        $sID = $this->most_recent_entry()->streamId + 1;
        echo "Stream id icremented \n"; 
      }
      $sense->streamID = $sID;
      $id = $sID;
      $typeSize = count($sense->type);
      $fname = "ChatApp/dataFiles/".$sense->sensorID."_".$sense->type[0].$sense->type[$typeSize-1]."_".$sense->streamID.".csv";
      $sense->fileName = $fname;
      $t = implode('', $sense->type);
      $g = $sense->type[0];
      $query = "INSERT INTO Datasets (userId, sensorId, type, typeGen, units, humanSubjectType, source, fileName, streamID) VALUES ('$sense->userID', '$sense->sensorID', '$t', '$g', '$sense->units', '$sense->humanST', 'app', '$fname', '$sID')";
      echo "Before insertion into database"; 
      $result = mysqli_query($this->conn, $query) or die(mysqli_error($this->conn)."\n");
      //echo "Query into Datasets to create new sensor row is complete\n"; 
    }
    $sense->sensorFile = "ChatApp/dataFiles/".$sense->sensorID."_".$sense->type[0]."_".$id.".csv";
    $client->send($id);		//send streamID to tablet for later use
    echo "\nMessage has been sent to the tablet post db entry\n";
  }
  

/*************************************************************************************************
Function below is used to upload analysis results to the Analysis table. Important for tracking 
previous analysis in the web portal for the user's convenience. 
*************************************************************************************************/

  public function uploadResults($streamID, $sensorID, $sensorType, $userEmail, $results, $algName) {
    $dataQuery = "select Datasets.dataId from Datasets 
                         WHERE Datasets.streamId='".$streamID."' 
                         AND Datasets.sensorId='".$sensorID."' 
                         AND Datasets.typeGen = '".$sensorType."'  
                         AND Datasets.userId = '".$userEmail."'";

   $dataId = $conn->query($dataQuery);

   while($row = $dataId->fetch_assoc()) {
      $dataIdValue = $row["dataId"];
   }

//TODO: Make the query more specific 
   $algIdQuery = "SELECT Distinct algId from Algorithms 
           where algName = '".$algName."' 
           AND   signalType = '".$sensorType."'";

   $algId = $conn->query($algIdQuery);

   while($row = $algId->fetch_assoc()) {
      $algIdValue = $row["algId"];
   }

   

   
$query = "Select userId from Accounts where email='".$userEmail."'";

$userID = $conn->query($query);


while ($row = $userID->fetch_assoc()) {
$userIdValue = $row["userId"] ;

}

echo $userIdValue;

$base = '/opt/stack/horizon/.blackhole/websockz/chat/ChatApp/userData';
$baseUser = $base . '/' . $userIdValue;
$userAnalysisPath  = $base . '/Analysis-Results';
$userAnalysisResultFile = 'Result_1.txt';

if (!file_exists($base)) {mkdir($base);}
if (!file_exists($baseUser)) {mkdir($baseUser);}
if (!file_exists($userAnalysisPath)) {mkdir($userAnalysisPath);}


$wholePath = $userAnalysisPath .'/'. $userAnalysisResultFile;

$i = 2;
while(file_exists($wholePath)) {
   $index = strpos($wholePath, "_");
   $wholePath = substr_replace($wholePath, strval($i), $index + 1, 1);
   $smallIndex = strpos($userAnalysisResultFile, "_");
   $userAnalysisResultFile = substr_replace($userAnalysisResultFile, strval($i), $smallIndex);
   $i = $i + 1;
}


$handle = fopen($wholePath, 'w') or die ('Cannot open file: '.$wholePath);
fwrite($handle, $results);


$insert = '/ChatApp/userData/' . $userIdValue . '/Analysis-Results' . $userAnalysisResultFile;
$query = "INSERT INTO Analysis (source, dataID, userID, algorithmID, resultFile) 
          VALUES ('App','".$dataIdValue."', '".$userEmail."', '".$algIdValue."', '".$insert."')";


$result = $conn->query($query);


}

/**************************************************************************************************
Create the splObjectStorage for the client currently communicating with the Ratchet websocket.
$this-clients is set as a spl object that stores objects which can then be identified with a unique hash id.
Useful because this program receives different sensors in async fashion it must uniquely tell apart. 
**************************************************************************************************/

    
  public function __construct() {
    $this->clients = new \SplObjectStorage;
    
    $servername = "localhost";
    $usr = "matt";
    $passw = "CSci@5657";
    
    $this->conn = mysqli_connect($servername, $usr, $passw, "csufml_db");
    if(mysqli_connect_errno())
    {
      die("Connection failed: " . $this->conn->connect_error . "\n");
    }
    else
      echo "Success in connecting to csufml_db\n";

    echo "initiating process\n";

  }


/**********************************************************************************************
onOpen launches when a client communicating with the Ratchet websocket first opens the connection.

**********************************************************************************************/

  public function onOpen(ConnectionInterface $conx) {
    // Store the new connection
    $this->clients->attach($conx);
    $this->count = 0;
    $this->countGoal = 300; 
    $this->timing_count = 0;
    $this->streamID = -1;
    $conx->send("you connected!");
    echo "Someone connected\n";

    $this->time_start = round(microtime(true) * 1000);  //gather time in microseconds
  }



/*********************************************************************************************
onMessage is a simple control structure that launches everytime the client sends a message to the 
Ratchet websocket. This message can be sensor data, or client commands, such as a request to 
launch real time analysis.

The message is stored in the $msg variable. The variable has two states.
One is a representation of either a sensor or a control message. 
    -It is structured as such if a control socket:  [ ["control"], ["user email"], ["Control message"] ]
    -It is structured as such if it is a sensor creation  message : [ ["user email"], [bluetooth address], [units], [human subject type]] 

The other representation is a single number that is stored into a csv file. 
*********************************************************************************************/
    
  public function onMessage(ConnectionInterface $from, $msg) {
    echo "$msg \n"; 
    //Return/create the unique hash id for the sepcific $from 
    $s = spl_object_hash($from);
    $msgArr = explode('~', $msg);
    //var_dump($msgArr); 
    //echo "A message is composed of: " ;
    //var_dump($msg); 
    if($msg == "Done") echo "$msg /n"; 
    //if($msg == "Do analysis") echo $msg . "\n";  

    if(count($msgArr) > 2) {
       echo "Sensor or control socket \n"; 
       if($msgArr[0] == "Control") {  //Handles control messages
          echo "Control message receivd \n";  
          if($msgArr[2] == "Do analysis"){
              echo "Doing analysis"; 
	     
              //$this->time_end_do_analysis = microtime(true) * 1000; 
              //echo "Epoch time after analysis $this->time_end_do_analysis  \n" ;
              //$execution_time = ($this->time_end_do_analysis - $this->time_start);                   
              //echo '<b>Total Execution Time For 100 Samples:</b> '.$execution_time.' milliseconds';



	       
	      
	      //First ensure that all of the sensors for the user have the minimum amount of entries required to do analysis.               
              $analysisIsReady = True; 
	      //Grab the appropriate user's sensors using the hash map of sensor keys
	      $email = $msgArr[1]; 
              $sensorComponentKeys = $this->emailAsKeyForSensorKeyArray[$email]; 
	      $sensorKeyOne = $sensorComponentKeys[0]; 

             // while(!$analysisIsReady) {
                // $analysisIsReady = True; 
                 foreach ($sensorComponentKeys as $key) { 
                   //echo $this->sensors["{$key}"]->sensorDataEntryCount ." \n" ;  
                    if($this->sensors["{$key}"]->sensorDataEntryCount < 100) {
                       $analysisIsReady = False;
                       echo "Analysis is not yet ready \n";  
                       //$from-send("Not enough data"); 
                      
                    } 
                 }
            //  }

              //If analysis can be performed, do so, otherwise, skip.
              if($analysisIsReady) {

	      //acquire the sensor(sensors, eventually) that are being used during this analysis 
	      $sense = $this->sensors["{$sensorKeyOne}"];


	      echo "$sense->sensorID \n";
	      echo "$sense->streamID \n";
	      //Store the timestamp to ensure unique filenames for realtime analysis calls (testing this method!*)
	      //I'll also likely want to store these into the Datasets table after returning from combineCSVs (?)
	      $timeStamp = date("Y-m-d H:i:s"); //the mysql dateTime format we use in the Datasets table    
	      echo "timestamp string check: $timeStamp \n";

              //create avalue for the NCSV samples program to use as the measure of the amount of samples the user wants to download 
              $sampleSize = 100; 

	      $timeStampArray = explode(" ", $timeStamp); //explode to separate into an array of two string elements
	      //creates a CSV file that contains the x,y,z data of the 100 previous samples and stores it in the dataOutputs directory   
	      $res = shell_exec("python ChatApp/combineNCSVDataSamples.py " . $sense->sensorID . " " . $sense->type[0] . " " . $sense->streamID. " " . $timeStampArray[0]. " ". $timeStampArray[1]. " ". $sampleSize);
	      echo "just finished combining csvs";


	      $sID = $sense->streamID; //grab stream ID of current sensor  
	      echo "\n Timestamp is: $timeStampArray[0] \n";
	      //In Doing analysis and filename to create 100 samples in is: ChatApp/dataOutputs/Lowout/00:06:66:E2:85:65_Low_9_ _.csv 
	      $fname = "ChatApp/dataOutputs/".$sense->type[0].$sense->streamID."out/".$sense->sensorID."_".$sense->type[0]."_".$sense->streamID."_". $timeStampArray[0]."_".$timeStampArray[1].".csv";
	      echo "\n In Doing analysis and file created to do n samples test on is named: $fname \n";
	      $t = implode('', $sense->type);
	      $g = $sense->type[0];


	      //insert the filepath of the 100 samples into the Datasets table (and make above a function since I am using it twice if this works)
	      //this will also track important information about the stream that created the csv that the filepath leads to 
	      $query = "INSERT INTO Datasets (userId, sensorId, type, typeGen, units, humanSubjectType, source, fileName, streamID, timestamp, done) VALUES (
					     '$sense->userID', 
					     '$sense->sensorID', 
					     '$t', 
					     '$g', 
					     '$sense->units', 
					     '$sense->humanST', 
					     'app', 
					     '$fname', 
					     '$sID',
					     '$timeStamp',
					      1)";

	      $result = mysqli_query($this->conn, $query) or die(mysqli_error($this->conn)."\n"); 

	      //send dummy message back
	      $from->send("Analysis Received");
	      echo "Analysis sent back to tablet \n";   
             }

         } else if ($msgArr[2] == "Done") {
	      echo "Dataset is now done \n";

	      //Grab the appropriate user's sensors using the hash map of sensor keys
	      $email = $msgArr[1];
	      $sensorComponentKeys = $this->emailAsKeyForSensorKeyArray[$email];
	      $sensorKeyOne = $sensorComponentKeys[0];
	      $sensorKeyTwo = $sensorComponentKeys[1]; 
	      $sensorKeyThree = $sensorComponentKeys[2]; 

	      //acquire the sensor(s, eventually) that are being used during this analysis 
	      $senseX = $this->sensors["{$sensorKeyOne}"]; 
	      $senseY = $this->sensors["{$sensorKeyTwo}"];
	      $senseZ = $this->sensors["{$sensorKeyThree}"];
	 
	      $fNameX = $senseX->fileName;
	      $fNameY = $senseY->fileName;
	      $fNameZ = $senseZ->fileName;

	      $sId = $senseX->streamID;

	      $query = "update Datasets set done = 1 where userId = '$senseX->userID' AND fileName = '$fNameX'  AND streamId = '$sId'";
	      $queryY = "update Datasets set done = 1 where userId = '$senseY->userID' AND fileName = '$fNameY'  AND streamId = '$sId'";
	      $queryZ = "update Datasets set done = 1 where userId = '$senseZ->userID' AND fileName = '$fNameZ'  AND streamId = '$sId'";

	     // echo "$fName is being set to done\n";

	      $result = mysqli_query($this->conn, $query) or die(mysqli_error($this->conn)."\n");
	      $result = mysqli_query($this->conn, $queryY) or die(mysqli_error($this->conn)."\n");
	      $result = mysqli_query($this->conn, $queryZ) or die(mysqli_error($this->conn)."\n");
	      

	      echo "Database should be updated\n";
      }
 
     } else {  //this block occurs if $msg[0] != "control". It is responsible for storing the incoming sensor information for later use.  
            $isSensorStored = false;
            if(empty($this->sensors))
              $isSensorStored = false;
            else{
              $isSensorStored = array_key_exists("{$s}", $this->sensors);
              }
           if(!$isSensorStored) {
              $this->clients->attach($from);

              $msgArr = explode('~', $msg);
              $newSensor = new Sensor();
              $newSensor->userID = $msgArr[0];
              $newSensor->sensorID = $msgArr[1];
              $newSensor->type = explode(' ', $msgArr[2]);
              $newSensor->units = $msgArr[3];
              $newSensor->humanST = $msgArr[4];
  
              $this->sensors["{$s}"] = $newSensor;
              var_dump($this->sensors);

              if(!array_key_exists($msgArr[0], $this->emailAsKeyForSensorKeyArray)) {
                 $this->emailAsKeyForSensorKeyArray[$msgArr[0]] = array();
              }
              array_push($this->emailAsKeyForSensorKeyArray[$msgArr[0]], $s);
              var_dump($this->emailAsKeyForSensorKeyArray);
              
              $this->db_entry_check($newSensor, $from);
            }
   }
}
  else {  //Data Streaming 
	      //echo $msg."\n";
              
	      $this->writeToCSV($this->sensors["{$s}"], $msg);
              $this->sensors["{$s}"]->sensorDataEntryCount += 1; 
	      $this->count = $this->count + 1; //to track the amount of packets incoming 
              //echo "Value of the sensorDataEntry for sensor axis x: " . $this->sensors["{$s}"]->sensorDataEntryCount;  
	      //echo "Current count: $this->count \n"; 
	      if($this->count == $this->countGoal) {
		      $this->time_end = round(microtime(true) * 1000);
		      $execution_time = ($this->time_end - $this->time_start);
		      //echo '<b>Total Execution Time For 300 Samples:</b> '.$execution_time.' milliseconds';
		      $execution_time = $execution_time/1000;
		     // echo 'Total Execution Time For 300 Samples: '. $execution_time. 'seconds';
              }

   }

} //Close onMessage


/******************************************************************************************************************************
/The Divider between the old on message flow and the new. New is on top, old is on botom 
/*****************************************************************************************************************************/


/*
 
    if(count($msgArr) > 2) { //hokey but works for now. If a message is from a control it will have 2 items. If it is from a 
      if($msgArr[1] != "Control") {
	    $isSensorStored = false;
	    if(empty($this->sensors))
	      $isSensorStored = false; 
	    else{
	      $isSensorStored = array_key_exists("{$s}", $this->sensors);
	    }
	    
	   
	    //if the sensor is not stored, we need to create the sensor object and insert it into the 
	    if(!$isSensorStored)
	    {
	      $this->clients->attach($from);
	      
	      $msgArr = explode('~', $msg);
	      $newSensor = new Sensor();
	      $newSensor->userID = $msgArr[0];
	      $newSensor->sensorID = $msgArr[1];
	      $newSensor->type = explode(' ', $msgArr[2]);
	      $newSensor->units = $msgArr[3];
	      $newSensor->humanST = $msgArr[4];
	      
	      $this->sensors["{$s}"] = $newSensor; 
	      var_dump($this->sensors); 

	      if(!array_key_exists($msgArr[0], $this->emailAsKeyForSensorKeyArray)) {
		 $this->emailAsKeyForSensorKeyArray[$msgArr[0]] = array(); 
	      }
	      array_push($this->emailAsKeyForSensorKeyArray[$msgArr[0]], $s);
	      var_dump($this->emailAsKeyForSensorKeyArray); 
	     
	      
	     
	      //variables inserted for testing. Delete when done. 
	      $senseType1 = $newSensor->type[0]; 
	      $senseType2 = $newSensor->type[1];
	      $senseType3 = $newSensor->type[2];
	      $senseType4 = $newSensor->type[3];  
	      echo "$newSensor->userID"."\n"; 
	      echo "$newSensor->sensorID"."\n";
	      echo "$newSensor->units";
	      echo "$senseType1";
	      echo " "; 
	      echo "$senseType2";
	      echo " ";  
	      echo "$senseType3";
	      echo " ";
	      echo "$senseType4";
	      echo " ";
	      echo "$newSensor->humanST";

	      $this->db_entry_check($newSensor, $from);
            }
      } 
    } //end of check forif it is a control socket 
    else if($msg == "Do analysis"){

	    //$this->time_end_do_analysis = microtime(true) * 1000; 
      //echo "Epoch time after analysis $this->time_end_do_analysis  \n" ;
      //$execution_time = ($this->time_end_do_analysis - $this->time_start);
      //echo '<b>Total Execution Time For 100 Samples:</b> '.$execution_time.' milliseconds';

      echo "Doing analysis";  
      //trigger analysis, "snapshot" files, combine data
      
      //acquire the sensor(s, eventually) that are being used during this analysis 
      $sense = $this->sensors["{$s}"]; 
      
      
      echo "$sense->sensorID \n";
      echo "$sense->streamID \n"; 
      //Store the timestamp to ensure unique filenames for realtime analysis calls (testing this method!*)
      //I'll also likely want to store these into the Datasets table after returning from combineCSVs (?)
      $timeStamp = date("Y-m-d H:i:s"); //the mysql dateTime format we use in the Datasets table    
      echo "timestamp string check: $timeStamp \n"; 
        
      $timeStampArray = explode(" ", $timeStamp); //explode to separate into an array of two string elements
      //creates a CSV file that contains the x,y,z data of the 100 previous samples and stores it in the dataOutputs directory   
      $res = shell_exec("python ChatApp/combineNCSVDataSamples.py " . $sense->sensorID . " " . $sense->type[0] . " " . $sense->streamID. " " . $timeStampArray[0]. " ". $timeStampArray[1] );
      echo "just finished combining csvs"; 
       
    
      //testing file creation of 100 samples with new edits in the combineCSVFile 
      //$fileResult =  shell_exec("python3 ChatApp/fileReader.py " . $sense->type[0] ." ". $sense->streamID ." ". $sense->sensorID ." ". $timeStampArray[0] ." ". $timeStampArray[1] );
      //echo $fileResult; 
      //not sure if best place to do this, but for now let's try it
      $sID = $sense->streamID; //grab stream ID of current sensor  
      //$sensetype = $sense->type;
      echo "\n Timestamp is: $timeStampArray[0] \n"; 
      //double check this line
      //In Doing analysis and filename to create 100 samples in is: ChatApp/dataOutputs/Lowout/00:06:66:E2:85:65_Low_9_ _.csv 
      $fname = "ChatApp/dataOutputs/".$sense->type[0].$sense->streamID."out/".$sense->sensorID."_".$sense->type[0]."_".$sense->streamID."_". $timeStampArray[0]."_".$timeStampArray[1].".csv"; 
      echo "\n In Doing analysis and file created to do n samples test on is named: $fname \n"; 
      $t = implode('', $sense->type);
      $g = $sense->type[0];


      //insert the filepath of the 100 samples into the Datasets table (and make above a function since I am using it twice if this works)
      //this will also track important information about the stream that created the csv that the filepath leads to 
      $query = "INSERT INTO Datasets (userId, sensorId, type, typeGen, units, humanSubjectType, source, fileName, streamID, timestamp, done) VALUES (
                                     '$sense->userID', 
                                     '$sense->sensorID', 
                                     '$t', 
                                     '$g', 
                                     '$sense->units', 
                                     '$sense->humanST', 
                                     'app', 
                                     '$fname', 
                                     '$sID',
                                     '$timeStamp',
                                      1)";
      
      $result = mysqli_query($this->conn, $query) or die(mysqli_error($this->conn)."\n");
     

 
      //$nbRes = shell_exec("python3 ChatApp/NaiveBayes.py " . $sense->sensorID  . " " . $sense->type[0] . " " . $sense->streamID . " ". $timeStampArray[0] . " " . $timeStampArray[1]); 
      //$from->send("NaiveBayes: " . $nbRes );
      $from->send("Result is: " . 5); 
      echo "Analysis sent back to tablet \n";
    }  


 
    else if($msg == "Done") {
      echo "Dataset is now done \n"; 
      $sense = $this->sensors["{$s}"]; 
      $fName = $sense->fileName;
      $sId = $sense->streamID; 
      $query = "update Datasets set done = 1 where userId = '$sense->userID' AND fileName = '$fName'  AND streamId = '$sId'";  
      echo "$fName is being set to done\n";    
      
      $result = mysqli_query($this->conn, $query) or die(mysqli_error($this->conn)."\n");
      echo "Database should be updated\n"; 
   }
    else
    {
      //echo $msg."\n";
      $this->writeToCSV($this->sensors["{$s}"], $msg);
      $this->count = $this->count + 1; //to track the amount of packets incoming 
      //echo "Current count: $this->count \n"; 
      if($this->count == $this->countGoal) {
      $this->time_end = round(microtime(true) * 1000); 
      $execution_time = ($this->time_end - $this->time_start);
      //echo '<b>Total Execution Time For 300 Samples:</b> '.$execution_time.' milliseconds';
      $execution_time = $execution_time/1000; 
     // echo 'Total Execution Time For 300 Samples: '. $execution_time. 'seconds';
      }
    }
  }
*/ 
  public function onClose(ConnectionInterface $conx) {
    $this->clients->detach($conx);
    $s = spl_object_hash($conx);
    $sen = $this->sensors["{$s}"];
    $fileName = "ChatApp/dataOutputs/".$sen->type[0].$sen->streamID."out/".$sen->sensorID."_".$sen->type[0]."_".$sen->streamID. ".csv";
    $t = implode('', $sen->type);
    $g = $sen->type[0];  
    $sID = $sen->streamID;  
    if(!file_exists($fileName)){
      $result = shell_exec("python ChatApp/combineCSVs.py " . $sen->sensorID . " " . $sen->type[0] . " " . $sen->streamID);
      echo "Combined CSV of all x,y,z files  created \n";
      $query = "INSERT INTO Datasets (userId, sensorId, type, typeGen, units, humanSubjectType, source, fileName, streamID, public, done) VALUES (
                                     '$sen->userID', 
                                     '$sen->sensorID', 
                                     '$t', 
                                     '$g', 
                                     '$sen->units', 
                                     '$sen->humanST', 
                                     'app', 
                                     '$fileName', 
                                     '$sID',
                                      1,
                                      1)";

      $result = mysqli_query($this->conn, $query) or die(mysqli_error($this->conn)."\n");
 
    }

    //$result = shell_exec("python ChatApp/writeCSV2.py ".$sen->sensorID.' '.$sen->type[0].' '.$sen->streamID);



    echo "Someone left";
  }

  public function onError(ConnectionInterface $conx, \Exception $e) {
    echo "An error occurred: {$e->getMessage()}\n";

    $conx->close();
  }
}
?>
