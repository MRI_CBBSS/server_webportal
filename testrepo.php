<?php
 /*
 Purpose: A user can upload algorithms and datasets, perform analysis on their stored datasets using an algorithm of their choice. Additionally, 
          the repository displays the user's uploaded datasets and algorithms, as well as their previous analysis results. A user is able to make
          their uploaded data or algorithms public, so that other users have access to them. A user can also download other users' data and 
          algorithms. A user can also donwload their own analysis results, and delete them if desired.

 */
  include "database.php";
 

  /*
   PHP here accomplishes basic setup. Ensure user is logged in, connect to database, perform queries that will be useful in the html of the page.
   First queries will gather data that will populate the analysis tool on the Analysis tab.

   TODO: The way in which this page, and accompanying scripts, handles user uploaded datasets and algorithms in populating the data for the 
         analysis tool needs to be made more dynamic. 
  */
 
  //Check if user has access to the page
  session_start(); 
  if(!isset($_SESSION['logged_in'])){
    session_unset();
    $_SESSION['logged_in'] = false;
  }
  if(!$_SESSION['logged_in']){
    header('Location: index.html');
    die("Permission Denied: Not Logged In");
  }  
  else 
   {
    //If user has access, begin downloading list of information available for computation
    $conn = mysqli_connect(DB_HOST, DB_USER, DB_PSWD, DB_NAME);
    $userEmail = $_SESSION["email"];
    
    if(!$conn) {
      echo "DB not connecting"; 
      die("Error: Could not connect to database"); } 
    else{//Grab the list of all sensors that are being used within the Datasets table  
      
 
      $q = "SELECT typeGen FROM Datasets WHERE userId = '".$userEmail."' OR public = 1 ORDER BY userId = '".$userEmail."' DESC, public = 1 DESC";
      //$q = "SELECT typeGen FROM Datasets WHERE public = 0 OR public = 1 ORDER BY public = 0 DESC, public = 1 DESC";
      $result = $conn->query($q);
      $sensorTypes = [];
      if($result->num_rows > 0){
        while($row = $result->fetch_assoc()){
          array_push($sensorTypes, $row["typeGen"]);
        }
	$sensorTypes = array_unique($sensorTypes);
        //die($resData[0]);
      }
      else
        die("hello number one");

      $algNames = [];
      foreach($sensorTypes as $name){
        $q = "SELECT algName,purpose,filePath FROM Algorithms WHERE signalType = '".$name."' AND (public = 1 OR ownerID = '".$userEmail."') ORDER BY ownerID = '".$userEmail."' DESC, public = 1 DESC";
	$result = $conn->query($q);
        if($result->num_rows > 0){ 
          
	  while($row = $result->fetch_assoc()){
            //get string position of first occurrence of a dot. Use that as the index. Pass in the substring from dot position to end of string as the extension 
            $path = $row["filePath"]; 
            $index = strpos($path, '.'); 
            $extension = substr($path, $index); 
	    array_push($algNames, $row["algName"]." (Purpose: ".$row["purpose"].")"."| $extension");
	  }
        }
      }
      $algNames = array_unique($algNames);
      $fileNames = [];
      foreach($sensorTypes as $name){
        $q = "SELECT fileName,streamId,sensorId,path FROM Datasets WHERE typeGen = '".$name."' AND (userId = '".$userEmail."' OR public = 1) ORDER BY userId = '".$userEmail."' DESC, public = 1 DESC";
	$result = $conn->query($q);
	if($result->num_rows > 0){ 
	  while($row = $result->fetch_assoc()){
	    $entry = substr($row["fileName"], 18);
	    //array_push($fileNames, $entry." sensor: ".$name." streamID: ".$row["streamID"]);
            if($row["sensorId"] == "none") {
              array_push($fileNames, "Filename: " .$row["fileName"]); 
            } else {
	    array_push($fileNames, "Sensor ID: ".$row["sensorId"]." | stream ID: ".$row["streamId"]);
	    //array_push($fileNames, $row["sensorID"]);
            }
	  }
	}
      }
      $fileNames = array_unique($fileNames);
    }
  }  



   /***************************************************************************************
   /Queries to fill the Algorithm Uploads and Data uploads tabs' list contents
   /First set is for Algorithms, second is for datasets
   /**************************************************************************************/ 


   //Set One-----------------------------------------------------------------------
   $query = "SELECT userId from Accounts WHERE email='".$userEmail."'"; 
   $result = $conn->query($query); 

   if($result->num_rows > 0 ) { 
      $row = $result->fetch_assoc(); 
      $uID = $row["userId"]; 
   }

   $algorithmTableResults = array();

   //TODO: Test with alternate users
   //TODO: Add timestamp when uploading algorithms in order to include in this seciton's alg table. Important b/c it also aids in making unique queries with this data. 
   $query = "select Accounts.email, Algorithms.algName, Algorithms.signalType, Algorithms.purpose, Algorithms.public, Algorithms.ownerID 
            FROM Accounts, (SELECT * from Algorithms WHERE ownerID='".$uID."' OR public='1') AS Algorithms 
            WHERE Accounts.userID=Algorithms.ownerID";
  
   $result = $conn->query($query); 

   if($result->num_rows > 0 ) { 
      while($row = $result->fetch_assoc()) {
         array_push( $algorithmTableResults, $row ); 
      }
   }

   //Grab the admin(basically, us the development team) algorithms and add to the result table 
   $query = "select * from Algorithms where ownerID='admin'";

   $result = $conn->query($query); 
 
   //pad the email with Admin and then push into algorithmTableResults array 
   if($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
         $row["email"] = "Admin";
          
         array_push($algorithmTableResults, $row); 
      }
   }

   //Set two--------------------------------------------------------------------
   $dataTableResults = array(); 

   $query = "SELECT userId, timestamp, fileName, typeGen, source, public, path FROM Datasets 
             WHERE userId = '".$userEmail."' OR public='1'"; 

   $result = $conn->query($query);

   if($result->num_rows > 0 ) {
      while($row = $result->fetch_assoc()) {
         array_push( $dataTableResults, $row );
      }
   }


   /*********************************************************************************************
   /Queries and pre-processing to fill the set of previous analysis results table 
   /********************************************************************************************/
   $algNameFileNameTable = array(); 

   $query = " select Algorithms.algName, Datasets.fileName, Analysis.timestamp, Analysis.resultFile, Analysis.incID  FROM Algorithms, Datasets, Analysis 
              WHERE  Algorithms.algId = Analysis.algorithmID 
              AND Datasets.dataId = Analysis.dataID  
              AND Analysis.userID = '".$userEmail."'
              AND Analysis.userID = Datasets.userId "; 
                                   
   $result = $conn->query($query);

   if($result->num_rows > 0 ) {
      while($row = $result->fetch_assoc()) { 
         //if /opt/stack/horizon/.blackhole is not in the string, add it then get file contents. If file does not exist after that, add null to result file 
         //TODO: Fix this part bc it won't work for files tha are not stored in userData subfolders 
         if(strpos($row["resultFile"] , "/opt/stack/horizon/.blackhole/") == false) { 
            $path = "/opt/stack/horizon/.blackhole/serverFiles/"; 
            $row["resultFile"] = $path . $row["resultFile"]; 
         }   
         
         if(file_exists($row["resultFile"])) {  
           $row["resultFile"] = file_get_contents($row["resultFile"]);  
         } else {
           $row["resultFile"] = "NA"; 
        }


         array_push( $algNameFileNameTable, $row );
      }
   }

  //Used to clean the spaces out of string elements that will be used in URLs
  //This is because spaces cannot be sent in URLs
  function replaceSpacesWithPluses($urlStringElement) {
    $cleanedURLStringElement = str_replace(" ", '+', $urlStringElement); 
    return $cleanedURLStringElement; 
  } 
   
?>  
<!DOCTYPE html>
<html lan="en-US">
  <head>
    <meta charset="utf-9">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Repository</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- jQuery | Bootstrap -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

    <!-- Other Files -->
    <link rel='stylesheet' href='styles/common_css.css'>
    <link rel="stylesheet" href="styles/repository_css.css">

    <script src='scripts/common_js.js'></script>
    <script  src="scripts/repository_js.js" ></script>

  </head>
  <body onload='checkUserID()'>
    <div  id='fill-window'>

      <div class="w-100 dark-theme">
        <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark">
          <a class="navbar-brand" href="https://sites.google.com/a/mail.fresnostate.edu/mri_cloud_sensor/">

            <div class="d-table" id='logoLeft'>
              <div class='d-table-cell align-middle' id='ll-content'>
                <div>
                  NSF
                </div>
                <div>
                  MRI
                </div>
              </div>
            </div>

            <div class="d-table" id='logoRight'>
              <div class="d-table-cell align-middle">
                <div>Cloud Based</div>
                <div class='float-left'>Body Sensor Systems</div>
              </div>
            </div>
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navBar" aria-controls="navBar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navBar">
            <div class="navbar-nav mr-auto">
              <a class="nav-item nav-link disabled" id='userDep-Account' href="#">Account <i class="fas fa-lock"></i></a>
              <a class="nav-item nav-link disabled active" id='userDep-Tool' href="#">Data & Alg Repository <i class="fas fa-lock"></i><span class="sr-only">(current)</span></a>
            </div>
            <div class="navbar-nav">
              <a class='nav-item nav-link' id='userDep' href='login.php'>Login</a>
            </div>
          </div>

        </nav>
      </div>
    </div>


      <!-- Main Body Container -->
      <main class='mt-4' id='push-footer'>
        <div class="container">
          <div class="row">

    	  <!-- SideNav bar -->
	  <div class="col-auto">
	    <div class="list-group" role='tablist' id='sideNav'>
	      <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center active" id='list-data' data-toggle='list' href='#data-content' role='tab' aria-controls="Data">
		Analysis
	      </a>
	      <a class="list-group-item d-flex justify-content-between align-items-center"  id='list-uploads' data-toggle='list' href='#uploads-content' role='tab' aria-controls="Uploads">
                Algorithm Uploads &nbsp;&nbsp;
                <span class="badge badge-dark badge-pill"><?php echo(count($algorithmTableResults)); ?></span>
              </a>
             <a class="list-group-item d-flex justify-content-between align-items-center" id="list-data-uploads" data-toggle='list' href="#data-uploads" role='tab' aria-controls="Data Uploads">
               Data Uploads &nbsp;&nbsp;
               <span class="badge badge-dark badge-pill"><?php echo (count($dataTableResults)); ?></span>
             </a>

	    </div>
	  </div>

	  <!-- <div class="tab"> -->
	    <!-- <button class="tablinks" onclick="openTab(event, 'tab1')" id="defaultOpen">Data</button> -->
	    <!-- <button class="tablinks" onclick="openTab(event, 'tab2')">Results</button> -->
	    <!-- <button class="tablinks" onclick="openTab(event, 'tab3')">Analysis History</button> -->
	  <!-- </div> -->
	  <div class="col">
	  <div class="tab-content">
	    <!-- Data tab content -->
	    <div id="data-content" class="tab-pane fade show active" role="tabpanel" aria-labelledby="list-data">
              <!-- Sensors -->
              <!-- <div class="col-xs-12 col-md-6 col-lg-4"> -->
	      <!-- <div class="col-xs-36 col-md-18 col-lg-12"> -->
	      <div class="w-100">
                <div class="box-group">
                  <a href="#previous-analysis-table">Jump To Previous Analysis Table </a>
                  <div class="box-group-title">
                    Sensors   
                    <div class="box-group-options">
                      <button type="button" class="btn btn-sm btn-outline-light d-inline-block" data-toggle="collapse" data-target="#sensSearch">
                        <i class='fas fa-search'></i>
                      </button>
                      <button id="sensBtn" class="btn btn-sm btn-outline-light d-inline-light" type="button" data-toggle="collapse" data-target="#sensWrapper">
                        <i class="fas fa-chevron-circle-down"></i>
                      </button>
                    </div>
                  </div>
                  <div id="sensSearch" class='collapse'>
                    <div class="dark-theme p-2">
                      <input placeholder="Search" onkeyup="updateButtons('#sensWrapper','sensList','.sensors',value)" type="text" class="form-control" placeholder="Username">
                    </div>
                  </div>

                  <div class='box-group-body'>

                    <div id="sensWrapper" class='collapse show'>
                      <ul id="sensList"class="optionContainer">
                        <!-- Same structure as the others -->
                        <?php foreach($sensorTypes as $name): ?>
                        <li>
                          <button type="button" id="<?php echo $name;?>" class='btn btn-sm btn-list rounded-0 sensors' onclick="updateList('sensors',this);">
                            <?php echo $name; ?>
                          </button>
                        </li>
                        <?php endforeach; ?>
                      </ul>
                    </div>

                  </div>
                </div>
                <!--  -->

              </div>

              <!-- Algorithms -->
              <!-- <div class="col-xs-12 col-md-6 col-lg-4"> -->
	      <!-- <div class="col-xs-18 col-md-9 col-lg-6"> -->
	      <div class="w-100">
                <div class="box-group">

                  <div class="box-group-title">

                    Algorithms
                    <div class="box-group-options">
                      <button type="button" class="btn btn-sm btn-outline-light d-inline-block" data-toggle="collapse" data-target="#algSearch">
                        <i class='fas fa-search'></i>
                      </button>
                      <button id="algBtn" class="btn btn-sm btn-outline-light d-inline-block" type='button' data-toggle="collapse" data-target="#algWrapper">
                        <i class="fas fa-chevron-circle-down"></i>
                      </button>
                    </div>
                  </div>
                  <div id="algSearch" class='collapse'>
                    <div class="dark-theme p-2 ">
                      <!-- updateButtons update the list of buttons based on user's search -->
                      <input placeholder="Search" onkeyup="updateButtons('#algWrapper','algList','.algorithms',value)" type="text" class="form-control" placeholder="Username">
                    </div>
                  </div>
                  <div class='box-group-body'>

                    <div id="algWrapper"class='collapse'>
                      <ul id="algList"class="optionContainer">
                        <!-- Insertion of buttons with php  -->
                        <?php foreach($algNames as $name): ?>
                        <li>
                          <!-- updateList disabled the buttons of the same category and places the selected button in the summary location -->
                          <button type="button" id="<?php echo $name;?>" class='btn btn-sm btn-list rounded-0 algorithms' onclick="updateList('algorithms',this)">
                            <?php echo $name; ?>
                          </button>
                        </li>
                        <?php endforeach; ?>
                      </ul>
                    </div>

                  </div>
                </div>
              </div>


              <!-- Data -->
              <!-- <div  class='col-xs-12 col-md-6 col-lg-4'> -->
	      <!-- <div class="col-xs-18 col-md-9 col-lg-6"> -->
	      <div class="w-100">
                <div class="box-group">

                  <div class="box-group-title">
                    Data
                    <div class="box-group-options">
                      <button type="button" class="btn btn-sm btn-outline-light d-inline-block" data-toggle="collapse" data-target="#dataSearch">
                        <i class='fas fa-search'></i>
                      </button>
                      <button id="datBtn" class="btn btn-sm btn-outline-light d-inline-block" type="button" data-toggle="collapse" data-target="#dataWrapper">
                        <i class="fas fa-chevron-circle-down"></i>
                      </button>
                    </div>
                  </div>
                  <div id="dataSearch" class='collapse'>
                    <div class="dark-theme p-2 ">
                      <input placeholder="Search" onkeyup="updateButtons('#dataWrapper','dataList','.data',value)" type="text" class="form-control"  placeholder="Username">
                    </div>
                  </div>

                  <div class='box-group-body'>

                    <div id="dataWrapper" class="collapse">
                      <ul id="dataList"class="optionContainer" >
                        <!-- Same as algorithm button insertion -->
                        <?php foreach($fileNames as $name): ?>
                        <li>
                          <button type="button" id="<?php echo $name;?>" class='btn btn-sm btn-list rounded-0 data' onclick="updateList('data',this)">
                            <?php echo $name; ?>
                          </button>
                        </li>
                        <?php endforeach; ?>
                      </ul>
                    </div>

                  </div>
                </div>
                <!--  -->

              </div>

              <!-- Summary -->
              <!-- <div id="summary" class='col-xs-12 col-md-6 col-lg-4 my-lg-3'> -->
	      <!-- <div id="summary" class="w-25"> -->
	      <div id="summary" class="w-100">
                <div class="box-group">

                  <div class="box-group-title">
                    Summary
                  </div>

                  <div class='box-group-body box-body-border'>

                    <div id="sumWrap" class="container">
                      <!-- Summary of buttons selected inserted below -->
                      <div id="sumSection" class="light-theme">

                      </div>
                      <div class="text-right ">
                        <!-- Button options -->
                        <button type="button" class="btn btn-dark" onclick='runTest()'>Run Test</button>
                        <button  id='compute-btn' class="btn btn-dark" disabled onclick="compute()"> Compute </button>
                      </div>
                    </div>

                  </div>
                </div>
                <!--  -->

              </div>

              <!-- Results -->
              <!-- <div id='results' class='col-xs-12 col-lg-8 my-lg-3'> -->
	      <!-- <div id='results' class='w-75'> -->
	      <div id="results" class="w-100">
                <div class="box-group">

                  <div class="box-group-title">
                    Analysis Results
                  </div>

                  <div class='box-group-body box-body-border'>

                    <div id="resultWrap" class="container">
                      <!-- Result text inserted below -->
                      <div id="resSection">
                      </div>
                      <div class="text-right">
                        <button  class="btn btn-dark" onclick="consoleClean()"> Clear </button>
                      </div>
                    </div>

                  </div>
                </div>

              </div>


              <!-- This is the previous analysis table ---------------------------------------------------------------------->  
              <div class="box-group">
               <div class="overflow-auto">
                   <h3 id="previous-analysis-table">Previous Analysis Results </h3>
                     <table class="table table-striped table-sm">
                        <thead>
                           <tr>
                              <th scope="col">Timestamp            </th>
                              <th scope="col">Data File Name       </th>
                              <th scope="col">Algorithm Name       </th>
                              <th scope="col">Result               </th>
                              <th colspan="2"scope="col">Actions             </th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php $i = 0; foreach($algNameFileNameTable as $row):   ?>
                              <tr>
                                 <th scope="row" class=<?php echo "analysis-row-$i"?> ><?php echo $row["timestamp"]; ?>         </th>
                                 <td class=<?php echo "analysis-row-$i"?>             ><?php echo $row["fileName"]; ?>          </td>
                                 <td class=<?php echo "analysis-row-$i"?>             ><?php echo $row["algName"]; ?>           </td>
                                 <td class=<?php echo "analysis-row-$i"?>             ><?php echo $row["resultFile"]; ?>        </td>
                                 <td class=<?php echo "analysis-row-$i"?>             >
                                    <a class="btn btn-dark" href=<?php echo "serverFiles/downloadAnalysisResults.php?inc=".$row["incID"]; ?>>Download </a>
                                 </td>
                                 <td class=<?php echo "analysis-row-$i";?>><button onclick="deletePreviousAnalysis(event)"  class="btn btn-dark ml-1">Delete</button></td>
                             </tr>
                           <?php $i = $i + 1; endforeach; ?>
                        </tbody>
                     </table>

               </div> <!-- End of overflow auto div --> 
              </div>  <!-- End of box-group div --> 






            </div>

	    <!----------------------------------------------------------- Upload tab content -------------------------------------------------->
	    <div id='uploads-content' class="tab-pane fade" role='tabpanel' aria-labelledby="list-uploads">
                  <h4 class='title'>Algorithm Uploads</h4>
                  <p>
                     To upload an algorithm, include all of the files that are used to make it compile and execute. The edits required to make the uploaded 
                     algorithm run are minimal. Simply add system arguments to the driver file. These arguments will be used as filepaths to data that will be used for 
                     training a model and testing the model. The first system argument will be used for training data, and the second for testing data. 
                  </p>
                  
                  <!-- Section that allows users to continue uploading data -->
                  <div class="row">
                    <form class='col-sm-12 col-md-12 my-2' action="serverFiles/algorithmUploads.php" method="POST" enctype="multipart/form-data" id='fileUploadForm-Alg'>
                      <h5>Algorithm</h5>
                      <!--
                        hidden command.
                        hidden extra info. {'fileType':number}. As of now, 0 = algorithm. 1 = data.
                        hidden input file type triggered with fancier looking buttons.
                      -->
                      <div class="row">
                         <div class="col-sm-4">
                            <input class='d-none' type='text' name='cmd'      value='uploadFile'>
                         </div>
                         <div class="col-sm-4">
                            <input class='d-none' type='text' name='fileType' value='0'>
                         </div>
                      </div>
                      <input class='d-none' type="file" name="uploadFile-Alg"  id='uploadFile-Alg' >
 
                       <div class="row"> 
                          <div class="col-sm-6">   
                             Algorithm Name:   <input type="text" name="algorithm_name"><br>
                          </div>
                          <div class="col-sm-6">
                             Package Name:   <input type="text" name="package_name"><br>
                          </div>
                       </div> 

                       <div class="row">
                          <div class="col-sm-6">
                             Sensor:         <select name="sensor_name"> 
                                                 <option value="Low">Low</option>
                                                 <option value="Gyroscope">Gyroscope</option>
                                                 <option value="ECG">ECG</option>
                                                 <option value="EMG">EMG</option>
                                              </select>
                          </div>
                          <div class="col-sm-6">
                              Purpose:        <input type="text" name="purpose_name">
                          </div>
                       </div>

                        <div class="input-group mb-3 px-0">
                          <!-- label updated as file is chosen -->
                          <div class="input-group-prepend">
                             <div class="input-group-text w-20"  id='alg-label'>
                                Choose file
                             </div>
                          </div>
                           <!-- button that triggers input file type -->
                           <input class='btn btn-outline-dark form-control' multiple="multiple" type="file" value='Select File' id="uploadFileLauncher-Alg" name="algorithms[]">
                          <div class="input-group-append">
                             <!-- button that triggers submission of the file chosen -->
                             <input class='btn btn-outline-dark' type="submit" value="Upload" id='submitBtn-Alg'>
                          </div>
                        </div>
                    </form>
                  </div>

                  <!--==================================================Algorithm Table ========================================================-->
                  <div class="overflow-auto">                      
                     <table class="table table-striped"> 
                        <thead>
                           <tr>
                              <th scope="col">Email         </th> 
                              <th scope="col">Algorithm Name</th>
                              <th scope="col">Signal Type   </th>
                              <th scope="col">Purpose       </th>
                              <th scope="col">Status        </th>
                              <th scope="col">Action        </th> 
                              <th scope="col">Download      </th> 
                           </tr>
                        </thead> 
                        <tbody>
                           <?php $i = 0; foreach($algorithmTableResults as $row):   ?> 
                              <tr>  
                                 <td scope="row" class=<?php echo "alg-row-$i";?> ><?php echo $row["email"];                ?> </th>
                                 <td class=<?php echo "alg-row-$i";?>             ><?php echo $row["algName"];              ?> </th>
                                 <td class=<?php echo "alg-row-$i";?>             ><?php echo $row["signalType"];           ?> </td> 
                                 <td class=<?php echo "alg-row-$i";?>             ><?php echo $row["purpose"];              ?> </td>               
                                 <td class=<?php echo "alg-row-$i";?>             >
                                 <?php 
                                           if($row["public"] == 1) {
                                              echo "Contributed"; 
                                           } else { 
                                             echo " <select class='alg-row-$i'>
                                                       <option value='Contribute'> Contribute </option> 
                                                       <option value='Remove'> Remove     </option>
                                                    </select>"; 
                                           }  
                                 ?></td>
                                 <?php
                                    if($row["public"] == 1){
                                       echo "<td class='alg-row-$i'>NA</td>"; 
                                    }else { 
                                       echo "<td><button class='alg-row-$i' onclick='makePublicOrRemove(event, 1)'>Submit </button></td>"; 
                                    }
                                 ?>
                                <td><a href=<?php echo "serverFiles/downloadAlgorithms.php?algName=".replaceSpacesWithPluses($row["algName"]).
                                                       "&UID=".$row["ownerID"].
                                                       "&signalType=".$row["signalType"].
                                                       "&purpose=".replaceSpacesWithPluses($row["purpose"]).
                                                       "&public=".$row["public"];?>
                                             >Download</a>
                                </td>

                             </tr>
                           <?php $i = $i + 1; endforeach; ?> 
                        </tbody>
                     </table>
                 
                  </div>

                </div>

                <!-------------------------------------------------------------------------------------------->
                <!-- Data Uploads tab --> 
                <!-- ------------------------------------------------------------------------------------------>
                <div id='data-uploads' class="tab-pane fade" role='tabpanel' aria-laelledby="list-approved">
                   <!-- Data -->
                    <!-- Similar to algorithm process -->
                    <form class='col-sm-12 col-md-6 my-2' action="serverFiles/dataUploads.php" method="POST" enctype="multipart/form-data" id='fileUploadForm-Data'>
                      <h5>Data</h5>
                        <p>A user may upload, contribute, and download public datasets, as well as their own. </p>
                       <div class="row">
                         <div class="col-sm-4">
                            <input class='d-none' type='text' name='cmd'      value='uploadFile'>
                         </div>
                         <div class="col-sm-4">
                            <input class='d-none' type='text' name='fileType' value='1'>
                         </div>
                      </div>
                      <input class='d-none' type="file" name="uploadFile-Data"  id='uploadFile-Data' >

                       <div class="row">
                          <div class="col-sm-6">
                             Sensor:         <select name="sensor_name">
                                                 <option value="Low">Low</option>
                                                 <option value="Gyroscope">Gyroscope</option>
                                                 <option value="ECG">ECG</option>
                                                 <option value="EMG">EMG</option>
                                              </select>
                          </div>
                       </div>


        

 
                      <div class="input-group">

                        <div class="input-group-prepend col px-0">
                          <!-- FileName label -->
                          <div class="input-group-text w-100" id=data-label>
                            Choose file
                          </div>
                        </div>
                        <div class="input-group-append col-auto px-0">
                          <!-- Trigger file input  -->
                          <input class='btn btn-outline-dark' type="file" value='Select File' id="uploadFileLauncher-Data" name="data[]">
                        </div>
                      </div>
                      <div class="row mt-md-2">
                        <div class="col">
                          <!-- Submit File -->
                          <input class='btn btn-outline-dark' type="submit" value="Upload" id='submitBtn-Data'>
                        </div>
                      </div>
                    </form>


                <!--=============================================================Data Uploads Table================================================--> 
                <div class="overflow-auto">
                     <table class="table table-striped">
                        <thead>
                           <tr>
                              <th scope="col">User ID           </th>
                              <th scope="col">Timestamp         </th>
                              <th scope="col">File Name         </th>
                              <th scope="col">Sensor Type       </th>
                              <th scope="col">Source            </th>
                              <th scope="col">Status            </th>
                              <th scope="col">Action            </th>
                              <th scope="col">Download          </th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php $i = 0; foreach($dataTableResults as $row):   ?>
                              <tr>
                                 <td scope="row" class=<?php echo "data-row-$i";?> ><?php echo $row["userId"];              ?> </th> 
                                 <td class=<?php echo "data-row-$i";?>             ><?php echo $row["timestamp"];           ?> </th>
                                 <td class=<?php echo "data-row-$i";?>             ><?php echo basename($row["fileName"]);            ?> </td>
                                 <td class=<?php echo "data-row-$i";?>             ><?php echo $row["typeGen"];             ?> </td>
                                 <td class=<?php echo "data-row-$i";?>             ><?php echo $row["source"]               ?> </td>
                                 <td class=<?php echo "data-row-$i";?>             ><?php
                                           if($row['public'] == 1) {
                                              echo "Contributed";
                                           } else {
                                             echo " <select class='data-row-$i'>
                                                       <option value='Contribute'> Contribute </option> 
                                                       <option value='Remove'> Remove     </option>
                                                    </select>";
                                           }  
                                      ?>                                         </td>
                                <td>
                                   <?php 
                                      if($row['public'] == 1) 
                                         echo "NA";
                                      else 
                                         echo "<button class='data-row-$i' onclick='makePublicOrRemove(event, 2)'>Submit</button>";
                                   ?>
                                </td> 
                               <!-- <td> 
                                    <button type="submit" class=< echo "data-row-$i";?> onclick="downloadFile(event)">Download </button>
                                </td> -->
                                <td><a href=<?php echo "serverFiles/downloadFiles.php?email=".$row["userId"].
                                                       "&fileName=".$row["fileName"].
                                                       "&typeGen=".$row["typeGen"].
                                                       "&source=".$row["source"];?>
                                             >Download</a>
                                </td>
                             </tr>
                           <?php $i = $i + 1; endforeach; ?>
                        </tbody>
                     </table>

              </div>

           </div>                
                


	  </div>
	  </div>

<!-- end of tab content -->
        </div>
      </div>
    </main>
    <!-- Body Ends -->

    <footer id='theFooter'>
      <!-- <div class="container py-3" id='footerUpper'>
        <div class="row">
          <div class="col-12" id='sponsorImgs'>
            <div class="row">
              <div class="col-5 align-self-center ">
                <div class="logoBox">
                  <img id='fsLogo' src="imgs/Fresno_State_Logo.png" alt="College Logo"/>
                </div>
              </div>
              <div class="col-2 align-self-center ">
                <div class="logoBox">
                  <img class='' id='nsfLogo' src="imgs/nsflogo.png" alt='NSF Logo'/>
                </div>
              </div>
              <div class="col-5 align-self-center ">
                <div class="logoBox">
                  <img id='utLogo' src="imgs/ut-dallas-logo.jpg" alt="UT Dallas Logo"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> -->
      <div class="jumbotron" id="footerLinksWrap">
        <div class="container py-1">
          <div class="row align-items-center mb-2">

            <div class="col">
              <div class="row" id='footerLinks'>

                <div class="col">
                  <a href="http://www.csufresno.edu/">Fresno State</a>
                </div>
                <div class="col">
                  <a href="https://www.nsf.gov/">National Science Foundation</a>
                </div>
                <div class="col">
                  <a href="https://www.utdallas.edu/">UT Dallas</a>
                </div>
                <div class="col">
                  <a href="https://sites.google.com/a/mail.fresnostate.edu/mri_cloud_sensor/">Project Page</a>
                </div>
              </div>
            </div>
          </div>
          <div class="row mt-2">
            <div class="col">
              Copyright &copy; 2016 - 2019 National Science Foundation
            </div>
          </div>
        </div>
      </div>
    </footer>
    </div>
  </body>
</html>
