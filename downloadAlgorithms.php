<?php 
/*
Purpose: This script will allow a user to download an algorithm or data file from the server. It receives information stored in the web portal's 
         data and algorithm tables and uses that to perform a query for the correct filepath. Once it has the filepath of the file, it is downloaded 
         and sent to the user for them to download onto their computer. 

IMP:     AT the moment this only supports single file downloads. So if a user wanted to download a java package, say as a zip file, they could not. 
         TODO: Implement zip file downloads of packages/algorithms composed of multiple files. 
         TODO: Security revisions.
*/
session_start(); 

if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']) { 
   //Connect to the database to perform a query. The goal of the query is to retrieve the filepath 
  //of the dataset that the user wishes to download. THis is necessary for the read_file php function
  $conn = new mysqli('localhost', 'matt', 'CSci@5657', 'csufml_db');

  if($conn->connect_error) {
    die("Error: Could not connect to database". $conn->connect_error);
  }
   
 
  //gather collected variables 
  $algName = $_GET["algName"]; 
  $signalType = $_GET["signalType"]; 
  $public = $_GET["public"]; 
  $purpose = $_GET["purpose"]; 
  $ownerID = $_GET["UID"];

  //echo $ownerID . " " . $signalType . " " . $purpose . " " . $algName . " ".  $public . "\n"; 

  
  //Retrieved filePath value will star with ChatApp
  if($ownerID == 'admin') { 
    $query = "SELECT filePath from Algorithms WHERE 
            ownerID = 'admin' AND 
            signalType = '".$signalType."' AND 
            public = '".$public."' AND 
            purpose = '".$purpose."' AND 
            algName = '".$algName."'"; 


     
   $result = $conn->query($query);

   if($result->num_rows > 0) {
     while ($row = $result->fetch_assoc()) {
       $targetFilePath = $row["filePath"];
     }
   } else {
       echo "Problem admin"; 
   }


   $targetFilePath = "/opt/stack/horizon/.blackhole/websockz/chat/".$targetFilePath;    

  }else { //Retrieved filePath value will begin with userData
 
  $query = "SELECT filePath from Algorithms WHERE 
            ownerID = '".$ownerID."' AND 
            signalType = '".$signalType."' AND 
            public = '".$public."' AND 
            purpose = '".$purpose."' AND 
            algName = '".$algName."'"; 
  
  $result = $conn->query($query); 

  if($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
      $targetFilePath = $row["filePath"]; 
    }
  } else {
      echo "Problem" ;   
  }  
  // echo "Value of filePath for nonadmin user: " . $targetFilePath;
   $targetFilePath = "/opt/stack/horizon/.blackhole/serverFiles/" . $targetFilePath; 
 }

 
  //the target file path will need to be edited to correctly navigate to any java file locations correctly.
  $targetFilePath = str_replace(" ", '/', $targetFilePath); 
  //echo "Post string edits: " . $targetFilePath; 


  if(file_exists($targetFilePath)) {
    header('Content-Type: application/octet-stream');
    header("Content-Disposition: attachment; filename=\"".basename($targetFilePath)."\"");
    readfile($targetFilePath);
    $conn->close();  
    exit();

  } else {
    echo "file does not exist will not be downloaded";
    die("Failure: File does not exist");
  }

   
  $conn->close();  
   
}


?>
