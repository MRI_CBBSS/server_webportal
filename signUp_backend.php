<?php
  // ini_set('display_errors', 1);
  // ini_set('display_startup_errors', 1);
  // error_reporting(E_ALL);
  include "databaseFunctions.php";

  function user_signUp($inputs){
    $errorNum = 0;
    $result = array('errorCount'=>$errorNum,'errors'=>array(),'errorIDs'=>array(),'success'=>false);
    $fName = $lName = $ind = $affil = $pos = $email = $pswd = $field = "";
    $noError = true;

    if ($_SERVER["REQUEST_METHOD"] == "POST"){
      #Validate First Name
      if (empty($inputs["fName"])){
        $noError = false;
        $result['errors'][] = "Please Enter Your First Name";
        $result['errorCount'] += 1;
        $result['errorIDs'][]='fName';
      }
      else{
          $fName = normalize($inputs["fName"]);
          if (!preg_match("/^[a-zA-Z ]*$/",$fName)) {
            $noError = false;
            $result['errors'][] = "First Name - Only letters and white space allowed";
            $result['errorCount'] += 1;
            $result['errorIDs'][]='fName';
          }
      }
      #Validate Last Name
      if (empty($inputs["lName"])){
        $noError = false;
        $result['errors'][] = "Please Enter Your Last Name";
        $result['errorCount'] += 1;
        $result['errorIDs'][]='lName';
      }
      else{
          $lName = normalize($inputs["lName"]);
          if (!preg_match("/^[a-zA-Z ]*$/",$lName)) {
            $noError = false;
            $result['errors'][] = "Last Name - Only letters and white space allowed";
            $result['errorCount'] += 1;
            $result['errorIDs'][]='lName';
          }
      }
      #Validate Email
      if (empty($inputs["email"])) {
        $noError = false;
        $result['errors'][] = "Please Enter Your Email Address";
        $result['errorCount'] += 1;
        $result['errorIDs'][]='email';
      }
      else {
          $email = normalize($inputs["email"]);
          if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $noError = false;
            $result['errors'][] = "Invalid Email Address";
            $result['errorCount'] += 1;
            $result['errorIDs'][]='email';
          }
      }
      #Validate Password
      if(empty($inputs["pswd"])){
        $noError = false;
        $result['errors'][] = "Please Enter Your Password";
        $result['errorCount'] += 1;
        $result['errorIDs'][]='pswd';
      }
      elseif(empty($inputs["pswd_check"])){
        $noError = false;
        $result['errors'][] = "Please Re-Enter Your Password";
        $result['errorCount'] += 1;
        $result['errorIDs'][]='pswd_check';
      }
      elseif($inputs['pswd'] != $inputs['pswd_check']){
        $noError = false;
        $result['errors'][] = "Passwords Do Not Match";
        $result['errorCount'] += 1;
        $result['errorIDs'][]='pswd';
        $result['errorIDs'][]='pswd_check';
      }
      else{
        $pswd = normalize($inputs["pswd"]);
      }
      #Validate Industry
      if (empty($inputs["ind"])){
        $noError = false;
        $result['errors'][] = "Please Enter Your Industry";
        $result['errorCount'] += 1;
        $result['errorIDs'][]='ind';
      }
      else{
          $ind = normalize($inputs["ind"]);
          if (!preg_match("/^[a-zA-Z ]*$/",$ind)) {
            $noError = false;
            $result['errors'][] = "Industry - Only letters and white space allowed";
            $result['errorCount'] += 1;
            $result['errorIDs'][]='ind';
          }
      }
      #Validate Field
      if (empty($inputs["field"])){
        $noError = false;
        $result['errors'][] = "Please Select a Field";
        $result['errorCount'] += 1;
        $result['errorIDs'][]='field';
      }
      else{
        $field = normalize($inputs["field"]);
      }

      #Validate Affiliation
      if (empty($inputs["affil"])){
        $noError = false;
        $result['errors'][] = "Please Enter Your Affiliation";
        $result['errorCount'] += 1;
        $result['errorIDs'][]='affil';
      }
      else{
          $affil = normalize($inputs["affil"]);
          if (!preg_match("/^[a-zA-Z ]*$/",$affil)) {
            $noError = false;
            $result['errors'][] = "Affiliation - Only letters and white space allowed";
            $result['errorCount'] += 1;
            $result['errorIDs'][]='affil';
          }
      }
      #Validate Position
      if (empty($inputs["pos"])){
        $noError = false;
        $result['errors'][] = "Please Select a Position";
        $result['errorCount'] += 1;
        $result['errorIDs'][]='pos';
      }
      else{
        $pos = normalize($inputs["pos"]);
      }
      if($noError){
          #Check if email already registered in database
          $exists = db_exists('Accounts',['email'=>$email]);

          if(!$exists){
              $values = array(
                'firstName'=>$fName,'lastName'=>$lName,
                'email'   =>$email ,'password'=>$pswd,
                'industry'=>$ind   ,'affiliation'=>$affil,
                'position'=>$pos   ,'active' => 1, 'field' => $field
              );
              $success = db_insert('Accounts',$values,true);

              if($success){
                $request = array('firstName','lastName','userId');
                $userInfo = db_getInfo('Accounts',$request,['email',$email]);

            		$path = 'userData/'.$userInfo['userId'];
            		mkdir($path);

                $hash = md5(rand(0,1000));

                $_SESSION['logged_in'] = true;
                $_SESSION['fName'] = $fName;
                $_SESSION['lName'] = $lName;
                $_SESSION['email'] = $email;

                db_updateInfo('Accounts',['path'=>$path,'hash'=>$hash],['email',$email]);
                if($result['errorCount'] == 0){
                  $result['success'] = true;
                }
              }
              else{
                $result['errors'][] = "Registration Failed";
                $result['errorCount'] += 1;
              }
          }
          else{
            $result['errors'][] = "User Account Already Exists";
            $result['errorCount'] += 1;
          }
        }
      }
      return $result;
    }
?>
