<?php 

/*
Purpose: Allow a user to contribute their datasets to the public userbase, or to remove their datasets from the database and the filesystem. 
         IMP: Once a dataset, or algorithm, have been contributed they cannot be deleted from tthe userbase.
TODO: Delete the selected dataset from the filesystem if it is deleted from the database. 
*/

session_start();

if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']){


$requestHandler = $_POST['dataResults'];
$selectedValue = $_POST['selectValue'];

$dataResults = json_decode($requestHandler);

$timeStamp = $dataResults[0];
$fileName = $dataResults[1];
$sensorType = $dataResults [2];
$source = $dataResults[3]; 

$conn = mysqli_connect('localhost', 'matt', 'CSci@5657', 'csufml_db');

//TODO: add userID to the query for a more unique algorithm identification. add a timestamp. 


if($selectedValue === 'Contribute') {
   $query = "UPDATE Datasets SET public=1 
             WHERE timestamp = '".$timeStamp."' 
                   AND fileName = '".$fileName."' 
                   AND typeGen='".$sensorType."'
                   AND source = '".$source."'";

   if ($conn->query($query) === TRUE )  {
        echo "success ";
   } else {
        echo "Fail";
   }

} else {

  $query = "DELETE from Datasets 
                   WHERE timestamp = '".$timeStamp."' 
                   AND fileName = '".$fileName."' 
                   AND typeGen='".$sensorType."'
                   AND source = '".$source."'";

  if ($conn->query($query) === TRUE )  {
        echo "success ";
   } else {
        echo "Fail";
   }


}
$conn->close();

}



?>
