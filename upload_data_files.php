<?php 
/*
Purpose: This function is called by the uploadFiles.php script. It will add the files to the filesystem in the user's directry in "UserData."
         It also calls the function that will upload the file information to the database. 
*/

include 'databaseFunctions.php'; 

function upload_data_files($fileType, $tmp_name_array, $file_name_array, $size_array, $userEmail, $sensor_name) {
   //Grab the size of the arrays [all arrays are the same size, so this approach works]
    $tmp_file_array_size = count($tmp_file_array);

    echo "Inside upload function ". "<br>"; 

    //Ensure file size is appropriate before creating the directories. If they are not exit the function and throw an error 
    for($i=0; $i < $tmp_file_array_size; $i++) {
       if($file_size_array[$i] > 500000) {
         throw new Exception('File Size Error');
       }
    }

    //Create the directory for the file to be inserted into
    $base = getUserPath($userEmail);// userData/userId
    $typeDir = $base.'/'.$fileType; // base/userData/Datasets
    $folderDir = "/opt/stack/horizon/.blackhole/serverFiles/".$typeDir; //base/userData/Datasets/datax
    

    if(!file_exists($base)){mkdir($base);}
    if(!file_exists($typeDir)){mkdir($typeDir);}

   for ($i=0; $i < count($tmp_name_array); $i++) {
     $folderDir = $folderDir  . "/". $file_name_array[$i]; 
     echo "Folder directory value is: $folderDir " . "<br>";
     if ( move_uploaded_file($tmp_name_array[$i],$folderDir) ) {
        echo "Data uploads to directory" . "<br>"; 
     } else {
        echo "Files not uploaded"; 
     }  
   }

   
   //upload to the database with the filepath and etc 
   include 'insert_data_to_db.php'; 
   uploadDataToDb($folderDir, $file_name_array, $userEmail, $sensor_name);  
}

?>
