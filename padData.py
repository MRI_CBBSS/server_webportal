import pandas as pd
import glob 
import numpy as np
from random import seed 
from random import random






#purpose it to easily pad data to existing csv files for testing on current version (3/21/19) of 
#Naive Bayes we are using.
#Goal: Read a csv and convert it into a dataframe in doing so. Convert that into a list.
#      Add the padding data by creating to more columns at the back to create a 5 column file
#      Export that as a csv into the system where the old file occupies. Maybe change name then
#      After it is in the directory go for renaming it somehow. 



first_frame = pd.read_csv('/opt/stack/horizon/.blackhole/websockz/chat/ChatApp/dataOutputs/Low12out/00:06:66:E2:85:65_Low_12.csv', header=None)
print("Frame before padding: ")
print(first_frame) 

first_frame_list = np.array(first_frame).tolist()

seed(1)



row_count = len(first_frame_list) 
for row in first_frame_list:
  for i in range(0,2):
    value = random()
    value = value * 10
    row.insert(0,value)

first_frame_list = np.array(first_frame_list)

print("The new aray after adding padding data")
print(first_frame_list)

test = pd.DataFrame(data=first_frame_list)  # 1st row as the column names

print("NEw data frame")
print(" ")
print(test)
test.to_csv(r'/opt/stack/horizon/.blackhole/websockz/chat/ChatApp/dataOutputs/Low12out/test.csv', index=False, header=False) 







