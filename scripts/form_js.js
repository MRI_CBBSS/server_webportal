//Contains Form Related Functions

// Serializes a form's elements into a json object. {'name':'value'}
function serializeForm(formID){
  formID = format_indicator('add','id',formID);
  var formJson = $(formID).serializeArray().reduce(function(obj, item) {
      obj[item.name] = item.value;
      return obj;
  }, {});
  return formJson;
}

//Remove the is-invalid class from all the inputs in the given form id
function removeInvalidClass(formID){
  formID = format_indicator('remove','id',formID);
  inputs = document.getElementById(formID).getElementsByTagName('input');
  for(i = 0; i < inputs.length; ++i){
      inputs[i].classList.remove('is-invalid');
      applyError(inputs[i].id,'');
  }
}

// Applies the error msg to the div with the id errorLoc
// Recieved id should be the id of the input it corresponds to
// Div displaying error should have the id [input id]-error
function applyError(errorLoc,msg){
  errorLoc = format_indicator('add','id',errorLoc);
  errorLoc += '-error';
  if($(errorLoc).length > 0){
    $(errorLoc).html(msg);
  }
}
//Adds bootstrap classes to change border colors
//correct = false -> red border
//correct = true -> green border
function applyColor(selector,correct){
  if(correct){
    $(selector).addClass('is-valid');
    $(selector).removeClass('is-invalid');
  }
  else{
    $(selector).addClass('is-invalid');
    $(selector).removeClass('is-valid');
  }
}

//apply is-invalid class to list of ids - adds red border
function applyInvalidClass(id_list){
  var length = id_list.length;
  for(var i = 0; i < length; ++i){
    id = format_indicator('add','id',id_list[i]);
    $(id).addClass('is-invalid');
  }
}

//Checks if all inputs inside of a given form id have no empty values
//If empty, element receives the class 'is-invalid'/red border and error message
//Returns true if no errors were found
function noEmptyInputs(formID){
  var success  = true;
  var errorMsg = '* Required';
  //double check form id is of format 'id' and not '#id'
  formID  = format_indicator('remove','id',formID);
  var inputs  = document.getElementById(formID).getElementsByTagName('input');
  var selects = document.getElementById(formID).getElementsByTagName('select');

  for(i = 0; i < inputs.length; ++i){
    if(inputs[i].value == ''){
      inputs[i].className += ' is-invalid';
      applyError(inputs[i].id,errorMsg);
      success = false;
    }
    else{
      //Remove any previous error
      inputs[i].classList.remove('is-invalid');
    }
  }
  for(i = 0; i < selects.length; ++i){
    if(selects[i].value == ''){
      selects[i].className += ' is-invalid';
      applyError(selects[i].id,errorMsg);
      success = false;
    }
    else{
      //Remove any previous error
      selects[i].classList.remove('is-invalid');
    }
  }
  return success;
}

//-------------------------------------------------------------------------//
// Following validity checks can call the applyColor() function and also
// apply the error type by setting doMore to true.

//Returns whether or not a given email string is a valid email
function validEmail(email, doMore=false, selector=''){
  selector = format_indicator('add','id',selector);
  var re    = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var valid = re.test(String(email).toLowerCase());
  var error = '';
  if(doMore && selector != '#'){
    applyColor(selector,valid);
    if(!valid){
      error = '* Invalid Email';
    }
    applyError(selector,error);
  }

  return valid;
}
//Checks if not empty, null, or undefined string
function notBlank(str, doMore=false, selector='') {
  selector = format_indicator('add','id',selector);
  var valid = !(!str || /^\s*$/.test(str));
  var error = '';

  if(doMore && selector != '#'){
    applyColor(selector,valid);
    if(!valid){
      error = '* Required';
    }
    applyError(selector,error);
  }
  return valid;
}
//Checks if no spaces in between and not empty
function validNoSpace(string, doMore=false, selector=''){
  selector = format_indicator('add','id',selector);
  var re = /^\S*$/;
  var noSpace  = re.test(String(string).toLowerCase());
  var notEmpty = notBlank(string);
  var valid = noSpace && notEmpty;
  var errorMsg = '';

  if(doMore && selector != '#'){
    applyColor(selector,valid);
    if(!notEmpty){
      errorMsg += '* Required'
    }
    else if(!noSpace){
      errorMsg += '* Can Not Contain Spaces.';
    }

    applyError(selector,errorMsg);
  }

  return valid;
}
//Compares the two given Id's to see if they match and are not empty
function pswdCheck(id_1,id_2, doMore=false){
  id_1     = format_indicator('add','id',id_1);
  id_2     = format_indicator('add','id',id_2);

  var equal     = $(id_1).val() == $(id_2).val();
  var noSpace1  = validNoSpace($(id_1).val());
  var noSpace2  = validNoSpace($(id_2).val());
  var notEmpty1 = $(id_1).val() != '';
  var notEmpty2 = $(id_2).val() != '';
  var errorMsg  = '';

  var valid = equal && noSpace1 && noSpace2 && notEmpty1 && notEmpty2;

  if(doMore){
    var errors = 0;
    applyColor(id_1,valid);
    if(!equal){
      errorMsg += '* Passwords Do Not Match.';
      errors += 1;
    }
    if(!notEmpty1){
      if(errors == 1){
        errorsMsg += '<br>'
      }
      errorMsg += '* Required.';
    }
    else if(!noSpace1){
      errorMsg += '* Can Not Contain Spaces.';
    }

    applyError(id_1,errorMsg);
  }
  return valid;
}
//-------------------------------------------------------------------------//


//Creates a POST request
//Serializes the data inside of the given form id and sends it
function form_createRequest(formID){
  var jsonObj = serializeForm(formID);
  $.post('serverFiles/requestHandler.php',jsonObj,function(result,status){
    if(status=='success'){
      console.log(result);
      result = JSON.parse(result);
      //By default, user is sent to the account page on sucess
      if(result['success']){
        window.location='account.php';
      }
      else{
        //Display any error messages that the server replies with
        displayErrors(result['errors']);
        //Add a red border around the IDs that the server found issues with
        applyInvalidClass(result['errorIDs']);
      }
    }
    else{
      alert('Post Error - Request Failed')
    }
  });
}
//specific submisson of form data to update password
function updatePswd(formID){
  removeInvalidClass(formID);
  //Check both passwords
  var noErrors = pswdCheck('#new-pswd-check','#new-pswd');
  if(noErrors){
    form_createRequest(formID);
  }
}

//standard form submission that only checks for empty inputs
function form_submit(formID){
  //Remove any 'is-invalid' classes
  removeInvalidClass(formID);
  //Check if the form contains no empty inputs
  var noErrors = noEmptyInputs(formID);
  if(noErrors){
    form_createRequest(formID);
  }
}
//specific registration form submission that requires validation of inputs
function form_submit_register(formID){
  //remove 'is-invalid'
  removeInvalidClass(formID);

  var checks = [];
  var noErrors = true;

  checks.push(  validNoSpace($('#fName').val(),true,'#fName') );
  checks.push(  validNoSpace($('#lName').val(),true,'#lName') );
  checks.push(  notBlank($('#ind').val(),true,'#ind')         );
  checks.push(  notBlank($('#affil').val(),true,'#affil')     );
  checks.push(  notBlank($('#pos').val(),true,'#pos')         );
  checks.push(  notBlank($('#field').val(),true,'#field')     );
  checks.push(  validEmail($('#email').val(),true,'#email')   );
  checks.push(  pswdCheck('#pswd_check','#pswd',true)         );
  checks.push(  pswdCheck('#pswd','#pswd_check',true)         );

  //Check all the checks to see if no errors were found
  var length = checks.length;
  for(var i = 0; i < length; i++){
    noErrors = noErrors && checks[i];
  }

  if(noErrors){
    form_createRequest(formID);
  }
}
