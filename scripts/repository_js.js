/*
Purpose: Contain all of the JQuery code that gives the repository section of the web portal necessary DOM manipulation, functionality,  or data gathering for sending information to the 
         backend with AJAX. 

*/
// Functions specific to the repository page

// For actions that require the page to be loaded beforehand
$(document).ready(function(){
  // Required to push down the footer towards the bottom of the screen
  $('#push-footer').css("padding-bottom",$("#theFooter").outerHeight()+30);
  // Display Initial Console Message
  consoleInit();
  // Onclick for summary buttons
  $("body").on("click",".removeOption",function(){
    var element = document.getElementById(this.id.slice(0,-4));
    var classes = element.className.split(" ");

    // Remove Summary Button Choice
    this.remove();

    // Enable button choices for removed button type (alg/sensor/data)
    // Categorical class name must be the last one
    // class='btn btn-theme removeOption algorithm'
    $("."+classes[classes.length-1]).prop("disabled",false);

    // Disable compute button if requirements to run algorithm are not met
    // COMBAK: should depend on more detailed info besides selection count
    $('#compute-btn').prop('disabled',true);
  });
});
//Update footer's location on resize
$(window).resize(function(){
  $('#push-footer').css("padding-bottom",$("#theFooter").outerHeight()+30);
});

// Simple test run computation
function runTest(){
  $('#resSection').html('Computing...');
  $.get('serverFiles/requestHandler.php',{'cmd':'runTest'},function(strResult,status){
    if(status == 'success'){
      $('#resSection').html(strResult);
    }
    else{
      alert('Test Failed');
    }
  });
}
// Called when search bar used - Displays matching buttons
function updateButtons(wrapper,locID,className,search){
  if(search != ''){
    var location   = document.getElementById(locID);
    var successors = location.children;
    var found      = false;

    search = search.toLowerCase();

    for(var i = 0; i < successors.length; ++i){
      var id = successors[i].children[0].id;
      //Display results if found
      if(id.toLowerCase().indexOf(search) == 0){
        document.getElementById(id).style.display = 'block';
        found = true;
      }
      else{
        document.getElementById(id).style.display = 'none';
      }
    }
    found?$(wrapper).collapse('show'):$(wrapper).collapse('hide');
  }
  else{
    $(wrapper).collapse('hide');
    $(className).css({display:'block'});
  }
}

//Disables buttons of the class className and makes a summary button
function updateList(className,e){
  var selector = format_indicator('add','class',className);
  $(selector).prop("disabled",true);
  $('#sumSection').html(
    $('#sumSection').html() +
    "<button id='"+e.id+"_cpy' class='btn btn-outline-dark btn-sm removeOption "+className +"'>"+
    e.id+" <i class='fas fa-times-circle'></button>"
  );
  var btnName = "none";
  var oldBtn = "none";
  if(className=="sensors"){
    btnName = "algBtn";
    oldBtn = "sensBtn";
  } else if(className == "algorithms"){
    btnName = "datBtn";
    oldBtn = "algBtn";
  } else if(className == "data"){
    btnName = "datBtn";
  }
  if(btnName != "none")
    document.getElementById(btnName).click();
  if(oldBtn != "none")
    document.getElementById(oldBtn).click();
  // COMBAK: update conditional check to something more detailed once requirements of data/alg/sensors are known
  if(document.getElementById('sumSection').children.length == 3){
    $('#compute-btn').prop('disabled',false);
  }
}

// Places summary section into a json object
function summaryToJson(){
  var buttons     = document.getElementById('sumSection').children;
  var sendingData = {"cmd":"runAlg"};

  for(var i = 0; i < buttons.length; ++i){
    //remove '_cpy' off id='someAlgName_cpy'
    var id = buttons[i].id.slice(0,-4);
    var element  = document.getElementById(id);
    var classes  = element.className.split(" ");

    // Make plural to singular: algorithms -> algorithm
    // Categorical class must be the last one
    // class='btn btn-outline-dark removeOption algorithms'
    var category = classes[classes.length-1];
    if(category.slice(-1) == 's'){
      category = category.slice(0,-1);
    }
    // Remove any returns
    sendingData[category] = id.replace(/\r?\n|\r/g,"");
  }
  return sendingData;
}

function openTab(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display="none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for(i = 0; i < tablinks.length; i++){
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Sends data over to the server
function compute(){
  //alert($('#sumSection').children()[0].textContent);
  // COMBAK: update conditional check to something more detailed once requirements of data/alg/sensors are known
  if($('#sumSection').children().length == 3){
    var jsonObj = summaryToJson();
    consoleWrite("Computing...");
    consoleScrollBottom(); 



    console.log($('#sumSection').children()[0].textContent);
    console.log($('#sumSection').children()[1].textContent);
    console.log($('#sumSection').children()[2].textContent);
   
    let algorithmInformation = $('#sumSection').children()[1].textContent; 
    let algorithmName = algorithmInformation.slice(0, algorithmInformation.indexOf("(")); 
    let purpose = algorithmInformation.slice(algorithmInformation.indexOf(":") + 2, algorithmInformation.lastIndexOf(")")); 
    let extension = algorithmInformation.slice(algorithmInformation.indexOf("|")+1 , algorithmInformation.length); 
    console.log(algorithmName);    
    console.log(purpose);
    console.log(extension);  



    
    var sensor = $('#sumSection').children()[0].textContent;
    sensor = sensor.replace(/\s/g, '');
    console.log("Here is the sensor: " + sensor); 
    var alg = $('#sumSection').children()[1].textContent;
    alg = alg.substr(0,alg.indexOf(' '));
    var btn3 = $('#sumSection').children()[2].textContent.split(' ');
     //alert(btn3);  alert(btn3.length);  

//Hardcoded sort of hack to fix when expanding the data for later
   if(btn3.length == 3) {
     var fileName = btn3[1]; 
     var sensID = "none"; 
     var strmID = "none"; 
    } else {
    var sensID = btn3[2];
    var strmID = btn3[6];
    var fileName = "none"; 
    //alert("../websockz/chat/");
    }    

/*
All of the ncessary variables to send: 
    -fileName is a replacement for sensID and strmID when the user uploaded a dataset 

*/

 
    var analysisResult; 
 
    console.time("Operation Tracking");
    $.post('serverFiles/requestHandler.php',{'cmd':'runAlg','sensor':sensor,'alg':alg,'sensID':sensID,'strmID':strmID,'fileN':fileName},  function(strResult,status){
      if(status=='success'){
        $('#resSection').html(strResult);
        console.timeEnd("Operation Tracking"); 
        analysisResult = strResult; 


       /* //Send necessary information to server so that it can store the results of the analysis in the analysis SQL table 
        $.post("serverFiles/uploadAlgorithmAnalysisResults.php" , {'algName': algorithmName, 'purpose': purpose, 'extension': extension, 'sensorType': sensor,  'sensorID': sensID, 'streamID': strmID, 'fileName': fileName }, function(status) {
           if(status == 'success') {
              alert("Info sent to Analysis Up page"); console.log("Analysis file returned ");
            } else {
              alert("Info not sent to anal page"); console.log("Analysis file not returned");
            }
        }); */  


      $.post("serverFiles/uploadAlgorithmAnalysisResults.php", {'algName': algorithmName, 'purpose': purpose, 'extension': extension, 'sensorType': sensor,  'sensorID': sensID, 'streamID': strmID, 'fileName': fileName, 'analResults': strResult}) 
            .fail(function(error) {alert("Failed to reach server");  })
            .done(function() { alert("Success analysisResults");});
       }         
      else{
        alert('Test Failed');
      }  
    }); 

     

    
   //alert("after post"); 
  }
  else{
    consoleWrite('Please Complete Your Selection');
    consoleScrollBottom();
  }
  consoleNewSection(); 

 
}

//----Console Functions----//

//Initial Hello Message
function consoleInit(){
  consoleClean();
  consoleWrite("Welcome to the CSUF tool! - Functionality In Progress");
}
//Write given text to console
function consoleWrite(txt){
  var cons = $('#resSection');
  cons.html(consoleHTML()+getTime()+txt+"<br>");
}
//Empty the console
function consoleClean(){
  $('#resSection').html('');
}
//Returns the current elements inside of the console
function consoleHTML(){
  return document.getElementById('resSection').innerHTML;
}
//Places a divider
function consoleNewSection(){
  $('#resSection').html(consoleHTML()+"====================================<br>")
}
//Scrolls the console to the bottom
function consoleScrollBottom(){
  $('#resSection').animate({scrollTop:$('#resSection').prop('scrollHeight')},'fast');
}

//document.getElementById("defaultOpen").click();








//****************************************************Code For The Upload Algorithms & Data Section******************************************************************//

//A function that creates a new editable text box designated for non-main modules that a user may use to paste their algorithms into 




//Object to store necessary information in order to send it over via JSON 
let fileNamesAndCode = {
   fileNames : [], 
   code      : []
};


function addModuleSection() {
   var $divObjectContainer = $("<div class='module_text_box_object'> <p class='module_file_name_container'>Filename: <span contenteditable='true' class='module_file_name_item'>...</span> </p> <div contenteditable='true' class='module_text_box'> Enter your code here. </div>  </div"); 

   $('#module_text_box_object_original').append($divObjectContainer); 
   console.log("Click Registered");     
} 

function processAlgorithmSections () {
   var moduleSectionDictionary = {}; 
   
   //Grab the classes responsible for storing the filenames and code
   //Place these values into arrays that are part of a JSON object 
   //Stringify that object and send it over to PHP  
    var fileNames = [] 
    var fileNameText = $(".module_file_name_item").each(function() {
          fileNames.push( $(this).text()); 
}); 
 

    var codeSections = []
    var codeSectiontext = $(".module_text_box").each(function() {
          codeSections.push( $(this).text()); 
}); 

    //Populate the dictionary 

    for(var  i=0; i < fileNames.length; i++) {
       moduleSectionDictionary[fileNames[i]] = codeSections[i]; 
}

    console.log(moduleSectionDictionary);
    
    fileNamesAndCode.fileNames = fileNames;    
    fileNamesAndCode.code = codeSections; 

    console.log(fileNamesAndCode); 

    var JSONReady = JSON.stringify(fileNamesAndCode); 
    console.log(JSONReady); 

    algorithmUploadToJSON(JSONReady);  
}

function algorithmUploadToJSON (JSONObject) {
 $.post('serverFiles/requestHandler.php',{'cmd':'uploadAlgorithm','JSON':JSONObject}, function(strResult,status){
      if(status=='success'){
        alert("Post successful"); 
        //$('#jtest').html(strResult);  
      }
      else{
        alert('Test Failed');
      }
    });


}

//a function to remove an algorithm from a user's database and file section or to make their algorithm public 
//one = algorithm
//two = dataset
function makePublicOrRemove(event, algOrData) {
  console.log(event.target.className);

  let classValue = event.target.className; 
  let classResults = $("."+ classValue); 
  console.log(classResults);
  console.log(classResults[0].innerHTML);
  let classResultsArray = []; 
  var i = 0; 

  if(algOrData == 1) {
  	for(i=0; i < 3; i++) {
     		classResultsArray.push(classResults[i].innerText); 
  	}
  } else {
        for(i=0; i < 5; i++) {
                classResultsArray.push(classResults[i].innerText);
        }

  }

  let JSONAlgResults = JSON.stringify(classResultsArray); 
  
  console.log(JSONAlgResults); 
  console.log(jQuery.isPlainObject(JSONAlgResults)); 


  let contributeOrRemoveSelect = $("select." + classValue);
  let selectedValue = contributeOrRemoveSelect.children("option:selected").val(); 
  console.log("Selected value is: " + selectedValue); 
  
   
  console.log(algOrData); 
 
  if(selectedValue === undefined) {
   	alert('Cannot remove or re-upload an uploaded algorithm'); 
  } else if(algOrData == 1) { 
 
  	$.post("serverFiles/contributeOrRemoveAlgorithms.php", {algResults: JSONAlgResults, selectValue: selectedValue} ) 
        	.fail(function(error) {console.log(error); }) 
        	.done(function() { alert("Success");});
  } else {
       $.post("serverFiles/contributeOrRemoveDatasets.php", {dataResults: JSONAlgResults, selectValue: selectedValue} )
                .fail(function(error) {console.log(error); })
                .done(function() { alert("Success");});

  
  }
  

}


//Not in use. Part of an experiment to get downloads working without using GET.
//Will swap current method for file downloads with  this route if time is available. 
function downloadFile(event) {
    console.log(event.target.className); 	
    console.log("Hellooo"); 
    let classValue = event.target.className;
    let classResults = $("."+ classValue);
    let dataTableValues = []; 
    for(let i=0; i < classResults.length - 2; i++) {
        dataTableValues.push(classResults[i].innerText); 
    }


   let JSONdataTableResults = JSON.stringify(dataTableValues); 

   $.post("serverFiles/downloadFiles.php", {dataTableValues: JSONdataTableResults})
       .fail(function(error) {console.log(error);})
       .done(function(data) {console.log(data);}); 
}



/******************************************************************************************
           Code for deleting analysis table results from table and databse
           TODO: Finish code for this                 
*******************************************************************************************/

function deletePreviousAnalysis(event) {
   let classValue = $(event.target).parent().className; 
   let classResults = $("." + classValue); 
   console.log(classResults); 
}







