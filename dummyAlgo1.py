#Need to call this from Chat.php, not from appOp.php

import csv
import sys
import glob
import itertools as IT
import os
import shutil
import time
import numpy as np 
from sklearn.naive_bayes import GaussianNB

#l = len(sys.argv) - 1
senseID = sys.argv[1] #the bluetooth address of the sensor being observed
senseType = sys.argv[2]	#accelerometer, gyroscope, etc.
streamID = sys.argv[3] #indicates the group of sensors a sensor belongs to

fs = []

for name in glob.glob('/opt/stack/horizon/.blackhole/websockz/chat/ChatApp/dataFiles/'+senseID+'*'+senseType+'*'+streamID+'*'):
  fs.append(name)
  #print(name)

if not fs:
  print ("No data files with those specifications exist.") 
  sys.exit()
else:
  for f in fs:
    n = '/opt/stack/horizon/.blackhole/websockz/chat/ChatApp/dataFiles/'+senseType+streamID+'/'
    if not os.path.exists(n):
      os.mkdir(n)
    try:
      shutil.copy(f, n)
    except:
      pass
    #except shutil.Error as e:
    #  print("hello")
    #except IOError as e:
    #  pass
      #print('Error: %s' % e.strerror)

lens = []
maindata = []
count = 0

for i in fs:
  with open(i,'r') as f:   # how it worked originally with Matt's code for python2 call: with open(i, "rb") as f: 
    reader = csv.reader(f)
    for row in reader:
      maindata.append(float(row[0]))

avg = sum(maindata)/len(maindata)
print ("Average of all data: "+str(avg))
print ("This is the main data: " + str( maindata)) 
#print len(maindata)


#n = 'ChatApp/dataOutputs/'+senseType+streamID+'out/'
#if not os.path.exists(n):
#  os.mkdir(n)

#with open(n+senseID+'_'+senseType+'_'+streamID+".csv", 'ab') as fd:
#  for i in range(len(maindata[0])):
#    newline = []
#    for c in range(count):
#      newline.append(np.float64(maindata[c][i]).item())
    #print newline
#    writer = csv.writer(fd)
#    writer.writerow(newline)
