<?php
  // Contains upload file function used in requestHandler.php

  // Required for getUserPath()
  include 'databaseFunctions.php';

  //Consider this a different function. Basically it will be a function for uploading multiple files. Will create another script for uploading single files next. This will be called in the upload_algort
  //-ithms file. It is my basic attempt at modularity. Will make it easier to debug when things go wrong. An abstract class would be pretty helpful for the uploading I think. 152 esque. Call a basic 
  //upload function and then it will call the appropriate object to do logic on. An Object Oriented approach. Would actually be pretty  interesting to try. I'll finish the basic version first though.
  function uploadPythonFiles($fileType,$tmp_file_array,$file_name_array,$file_size_array,$userEmail, $package_name, $purpose, $sensor){

    echo "Inside the script for Python files and working"; 
    //Grab the size of the arrays [all arrays are the same size, so this approach works]
    $tmp_file_array_size = count($tmp_file_array);


 
    //Set a cap on the file size and throw an exception if it is violated  
    for($i=0; $i < $tmp_file_array_size; $i++) {
       if($file_size_array[$i] > 30000) {
         throw new Exception('File Size Error');
       }
    }

    echo "After file cap"; 
    //Create the directory for the file to be inserted into
    $base = getUserPath($userEmail);// userData/userId
    $typeDir = $base.'/'.$fileType; // base/userData/userId/Algorithms
    $folderName = "Algorithm_1"; 
    $folderDir = $typeDir.'/'. $folderName; //base/userData/Algorithms/Algorithmx
  
    echo "After directories named"; 

    //create the base directories 
    if(!file_exists($base)){mkdir($base);}
    if(!file_exists($typeDir)){mkdir($typeDir);}

    echo "After directories created"; 

    $i = 2; 
    //Create a new directory if the current directory already exists. This wil house the Algorithm being inserted within the Algorithms folder. 
    while(file_exists($folderDir)) { 
       $index = strpos($folderDir, "_"); 
       $folderDir = substr_replace($folderDir, strval($i), $index + 1, 1); 
       $i = $i + 1; 
    } 

    echo "After renaming the directory"; 

    mkdir($folderDir); 
    echo "New directory just created: $folderDir"; 

    /********************************************************
    /In this section files are uploaded to a user's directory 
    /********************************************************/



       //Get rid of whitepace in file names to help with file processing and executing on the server 
       for($i=0; $i < $tmp_file_array_size; $i++) {

          $fileNameSpaceSeparatedList = explode(" ", $file_name_array[$i]); 
          $fName = ""; 
          for($p = 0, $j = count($fileNameSpaceSeparatedList); $p < $j; $p++){
             $fName.=$fileNameSpaceSeparatedList[$p]; 
          } 
          $file_name_array[$i] = $fName;  
       } 
      // echo "whitespace removed " ; 
       
       //Now loop through the tmp_file_array and add the files to the new algorithm folder just created 
       for($i=0; $i< $tmp_file_array_size;$i++) {
          $tempFolderDir = $folderDir .'/'.$file_name_array[$i];
          if(move_uploaded_file($tmp_file_array[$i], $tempFolderDir)) {
             echo "File created $tempFolderDir". "<br>";  
          } 
          
       }  

    /**********************************************************
    /In this section the main python file is uploaded to the database.
    /*********************************************************/

       //Insert the Driver file to the database. The reason to only push this is simpele. We use it to run the package contents. 
       include 'insert_alg_to_db.php'; 

       $dbUserId = db_getInfo('Accounts', ['userId'],['email', $userEmail]);
       $userId = $dbUserId['userId'];
       $pathToDriverFile = $folderDir . "/Driver.py"; 
       echo "To be uploaded to database: $pathToDriverFile "; 
        
       uploadAlgToDb("Driver.py", $sensor, $purpose, $userId, $pathToDriverFile, $userEmail); 
     
  

}  
?>
