<?php 
session_start();

if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
   //Connect to the database to perform a query. The goal of the query is to retrieve the filepath 
  //of the dataset that the user wishes to download. THis is necessary for the read_file php function
  $conn = new mysqli('localhost', 'matt', 'CSci@5657', 'csufml_db');

  if($conn->connect_error) {
    die("Error: Could not connect to database". $conn->connect_error);
  }

  $incID = $_GET["inc"];

  $query = "SELECT resultFile from Analysis WHERE incID='".$incID."'";

  $result = $conn->query($query); 

  if($result->num_rows > 0) {
     while ($row = $result->fetch_assoc()) {
       $targetFilePath = $row["resultFile"];
     }
   } else {
       echo "Problem admin";
   }
 
  //finish the whole filepath to result file 
  $targetFilePath = "/opt/stack/horizon/.blackhole/serverFiles/".$targetFilePath; 

  if(file_exists($targetFilePath)) {
    header('Content-Type: application/octet-stream');
    header("Content-Disposition: attachment; filename=\"".basename($targetFilePath)."\"");
    readfile($targetFilePath);
    $conn->close();
    exit();

  } else {
    echo "file does not exist will not be downloaded";
    $conn->close(); 
    die("Failure: File does not exist");
  }

  
  $conn->close();  
} 
?>
