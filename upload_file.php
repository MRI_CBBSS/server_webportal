<?php
/*
Purpose: Uploads algorithm files to the filesystem and calls a function to pload the information for that file to the database.

*/  // Contains upload file function used in requestHandler.php

  // Required for getUserPath()
  include 'databaseFunctions.php';

  //Consider this a different function. Basically it will be a function for uploading multiple files. Will create another script for uploading single files next. This will be called in the upload_algort
  //-ithms file. 
  function upload_file($fileType,$tmp_file_array,$file_name_array,$file_size_array,$userEmail, $package_name, $purpose, $sensor, $algorithm_name){

    //Grab the size of the arrays [all arrays are the same size, so this approach works]
    $tmp_file_array_size = count($tmp_file_array);


 
    //Ensure file size is appropriate before creating the directories. If they are not exit the function and throw an error 
    for($i=0; $i < $tmp_file_array_size; $i++) {
       if($file_size_array[$i] > 30000) {
         throw new Exception('File Size Error');
       }
    }


    //Create the directory for the file to be inserted into
    $base = getUserPath($userEmail);// userData/userId
    $typeDir = $base.'/'.$fileType; // base/userData/Algorithms
    $folderName = "Algorithm_1"; 
    $folderDir = $typeDir.'/'. $folderName; //base/userData/Algorithms/Algorithmx
    //$dir = $folderDir.'/'.$fName;     // base/userData/Algorithms/Algorithmx

    //create the base directories 
    if(!file_exists($base)){mkdir($base);}
    if(!file_exists($typeDir)){mkdir($typeDir);}


//    $info = pathinfo($dir);
  //  $ext  = $info['extension'];
    //$name = $info['filename'];

     $i = 2; 
    //Create a new directory if the current directory already exists. This wil house the Algorithm being inserted within the Algorithms folder. 
    while(file_exists($folderDir)) {
       $index = strpos($folderDir, "_"); 
       $folderDir = substr_replace($folderDir, strval($i), $index + 1, 1);
       $i = $i + 1; 
    }
    mkdir($folderDir); 


    //Now we want to discern if we are uploading a package or a single file. After which we will:
    //  a) Insert the single file into the directory we just created 
    //  b) Create a subdirectory with the package name and place everything but the file named "Driver" within it.Driver file stays at the top level. 
  
    //TODO: Remove whitespace in the file name for the single file upload case. 
 
    //Do Single File Upload if there is not multiple files
    if($tmp_file_array_size == 1) { 
      echo "This is file directory: ". $folderDir.' '.$file_name_array[0]; 
      $folderDir = $folderDir.'/'.$file_name_array[0];  
      if(move_uploaded_file($tmp_file_array[0],$folderDir)){
          echo "Works";  
      }

   
     //Test insert  
       include 'insert_alg_to_db.php';
       $dbUserId = db_getInfo('Accounts', ['userId'],['email', $userEmail]);
       $userId = $dbUserId['userId'];  
       uploadAlgToDb($algorithm_name, $sensor,$purpose, $userId, $folderDir, $userEmail);
       
     //return the filepath so we can use it in the compilation function-return in an array 
       $filePathArray = []; 
       $filePathArray[0] = $folderDir; 
       return $filePathArray; 
 
    
    } 

    else { //Do multiple file uploads otherwise 


       //Get rid of whitepace in file names to help with file processing and executing on the server 
       for($i=0; $i < $tmp_file_array_size; $i++) {

          $fileNameSpaceSeparatedList = explode(" ", $file_name_array[$i]); 
          $fName = ""; 
          for($p = 0, $j = count($fileNameSpaceSeparatedList); $p < $j; $p++){
             $fName.=$fileNameSpaceSeparatedList[$p]; 
            // echo "File name is" . $fName; 
          }
          
          $file_name_array[$i] = $fName;  
       //   echo "File name's renamed as: " . $file_name_array[$i]; 
       } 

       
       //Now create a new directory, as above. Insert the package name inside as another directory within. Place all non "Driver.java" files inside. 
       $packageDir = $folderDir.'/'.$package_name;                            //userData/15/Algorithms/Algorithm_x/package_name
       mkdir($packageDir); 
       $driverFileIndex = 0; 
       
       //to store the filepaths to be returned for later compilation 
       $javaFilePathsArray = []; 
 
       //Now loop through the tmp_file_array and add the files to the package directory 
       for($i=0; $i< $tmp_file_array_size;$i++) {
          if($file_name_array[$i] == "Driver.java" ) { 
             $driverFileIndex = $i; 
             continue;
          } 
          $tempPackageDir = $packageDir .'/'.$file_name_array[$i];
          if(move_uploaded_file($tmp_file_array[$i], $tempPackageDir)) {
            // echo "File created"; 
          } 
          
       }


       //Now insert the Driver File in the userData/15/Algorithms/Algorithms_x/Driver.java directory
       $folderDirTemp = $folderDir .'/'.$file_name_array[$driverFileIndex];  
       move_uploaded_file($tmp_file_array[$driverFileIndex], $folderDirTemp); 
       
       //Now return an array with two paths: 
       //    1. The path to the driver file at position 0.
       //    2. The path to the package directory at position 1. 
       array_push($javaFilePathsArray, $folderDir);
       array_push($javaFilePathsArray, $packageDir);  

/*
       $driverFileNameWithoutJavaEnding = $file_name_array[$driverFileIndex]; //important for launching the java files from the command line in requestHandler.php 
       $driverFileNameWithoutJavaEnding = chop($driverFileNameWithoutJavaEnding, ".java"); 
       $folderDir = $folderDir . "/ ". $driverFileNameWithoutJavaEnding; */ 
       $folderDir = $folderDir . ' ' . $file_name_array[$driverFileIndex]; 
       //Insert the Driver file to the database. The reason to only push this is simpele. We use it to run the package contents. 
       include 'insert_alg_to_db.php'; 

       $dbUserId = db_getInfo('Accounts', ['userId'],['email', $userEmail]);
       $userId = $dbUserId['userId'];

        
       uploadAlgToDb($algorithm_name, $sensor, $purpose, $userId, $folderDir, $userEmail); 
     
      //Now return the list of files to the main function for compilation in an array 
      return $javaFilePathsArray; 
        
    } 

  }
?>
