<?php 
/*
Purpose: This script allows users to either contribute their algorithms, aka make them public in the database, or remove their algorithms from the database and the filesystem.
IMP: Cannot remove algorithms that are already set to public. 
TODO: Delete the algorithm from the filesystem after deleting it from the database.
*/

session_start(); 

if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']){


$requestHandler = $_POST['algResults']; 
$selectedValue = $_POST['selectValue']; 

$algResults = json_decode($requestHandler); 

$algName = $algResults[0]; 
$signalType = $algResults[1]; 
$purpose = $algResults [2]; 


$conn = mysqli_connect('localhost', 'matt', 'CSci@5657', 'csufml_db');

//TODO: add userID to the query for a more unique algorithm identification. add a timestamp. 


if($selectedValue === 'Contribute') {
   $query = "UPDATE Algorithms SET public=1 
	     WHERE algName = '".$algName."' 
                   AND signalType = '".$signalType."' 
                   AND purpose = '".$purpose."'";  

   if ($conn->query($query) === TRUE )  {
   	echo "success "; 
   } else {
   	echo "Fail"; 
   }

} else {

  $query = "DELETE from Algorithms 
            WHERE  algName = '".$algName."' 
                   AND signalType = '".$signalType."' 
                   AND purpose = '".$purpose."'";

  if ($conn->query($query) === TRUE )  {
        echo "success ";
   } else {
        echo "Fail";
   }
  

}  
$conn->close();  

}
?> 
