<?php
  //for time tracking purposes [remove later]
  //$time_start = round( microtime(true) * 1000);

  include "database.php";
  //=========================================================
  // Main php file that handles most requests.Comments above each section explain functionality of each switch case 
  //=========================================================
  // Create/Resume current session
  session_start(); 
  //***************************** Handle POST Requests *****************************//  
  
  if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Check if a command is given 
    if(!empty($_POST['cmd'])){
    // echo "cmd received in Post method switch"; 
      switch($_POST['cmd']){
        //====================== Update Account Information ======================
        case 'updateAccount':
          include 'databaseFunctions.php';
          $info = array();

          $tempEmail = $_SESSION['email'];
          //Check if updateValues is given - checked checkboxes
          if(isset($_POST['updateValues'])){
            $values = $_POST['updateValues'];

            foreach($values as $value){
              //'id-value'
              $info  = explode('-',$value);
              $id    = $info[0];
              $value = $info[1];
              //conditional check for any string that has dashes inside as well
              //lastName-something-cool => lastname something-cool
              if(count($info) > 2){
                for ($i=2;$i < count($info); $i+=1) {
                  $value .= '-'.$info[$i];
                }
              }
              // If updating email, store it
              if($id == 'email'){
                $tempEmail = $value;
              }
              $info[$id] = $value;

            }
            // Update all information
            db_updateInfo('Accounts',$info,['email',$_SESSION['email']]);
            // Update new email
            $_SESSION['email'] = $tempEmail;
            header('location:../account.php');
          }
          //No Checked Checkboxes
          else{
            echo "<script type='text/javascript'>alert('No Selected Values To Update'); window.location='../account.php';</script>";
          }
          break;

        //====================== Update User's Password ======================
        case 'updatePswd':
          include 'databaseFunctions.php';

          // Check if no original password given
          if (!isset($_POST['og-pswd']) || empty($_POST['og-pswd'])) {
            echo json_encode(['errorIDs'=>['og-pswd'],'success'=>false,'errors'=>['Please Enter Your Password']]);
          }
          // Check if new password is given
          elseif(isset($_POST['new-pswd']) && isset($_POST['new-pswd-check'])){
            $newPswd = $_POST['new-pswd'];
            $chkPswd = $_POST['new-pswd-check'];
            $ogPswd  = $_POST['og-pswd'];

            // Make sure that the new password matches the password check
            if(!empty($newPswd) && $newPswd == $chkPswd){
              // Get Users Password from the database
              $db_data = db_getInfo('Accounts',['password'],['email',$_SESSION['email']]);
              // Verify that the passwords do match
              if(password_verify($ogPswd,$db_data['password'])){
                db_updateInfo('Accounts',['password'=>$newPswd],['email',$_SESSION['email']]);
              }
              else{
                echo json_encode(['errorIDs'=>['og-pswd'],'success'=>false,'errors'=>['Incorrect Password']]);
              }
            }
            else{
              echo json_encode(['errorIDs'=>['new-pswd','new-pswd-check'],'success'=>false,'errors'=>['Passwords Do Not Match']]);
            }
          }
          else{
            echo json_encode(['errorIDs'=>[],'success'=>false,'errors'=>['Missing Values']]);
          }
          break;
        //====================== Run Algorithm ======================//
        /*
           An imporant section. Allows a user to perform analysis on the Analysis section of the web portal. 
           TODO: Make choosing what kind of language to command more dynamic. RN handler only can tell if it is handling java or python files. 
           TODO: Make how the handler decides if file is an admin file or a user uploaded file more dynamic.
           TODO: Additionaly tests with user vs non-user uploaded algorithms. 
           TODO: Clean up the code in this section
        */
        case 'runAlg':
	  include "databaseFunctions.php";
          $processUser = posix_getpwuid(posix_geteuid());

     
          //Collect all the information needed to perform analysis
          $fileName = $_POST['fileN']; 
	  $sensor = $_POST['sensor'];
	  $alg = $_POST['alg'];
	  $sensID = $_POST['sensID'];
	  $strmID = $_POST['strmID'];

          //Prep file paths that will be joined with the sql filepath column to create a complete filepath
          //The native file path variable refers to the algorithms uploaded by the NSF team
          //The non native file path refers to the algorithms uploaded by the system users 
	  $nativeFilePath = "/opt/stack/horizon/.blackhole/websockz/chat/";
          $nonNativeFilePath = "/opt/stack/horizon/.blackhole/serverFiles/"; //reserved for a user's file 
          $sqlStoredFP = ""; 


	  //pull rest of needed data from repository.js and perform analysis here
	  if(!empty($sensor) && !empty($alg) && !empty($sensID) && !empty($strmID)){
	    //echo $strmID;
	    //echo $alg;

            //functions that access the database to query for information 
	    $dbAlg_data = db_getInfo('Algorithms',['filePath'],['algName',$alg]);
            $dbDataInfo = db_getInfo('Datasets', ['path'], ['fileName', $fileName]); 
            echo $dbDataInfo ; 
            echo $dbAlg_data ; 

            //stores the path which contains the data the user will be analyzing 
            $filePath = $dbDataInfo['path']; 
            echo $filePath;   

            //stores the path to the user's algorithm location. This is passed into the $runner variable to execute the program  
            $sqlStoredFP = $dbAlg_data['filePath']; 
            echo "Value of the stored value for file path is: $sqlStoredFP"; 
           

            //If the database filepath begins with a 'C' this indicates the file is stored at the 'ChatApp/..' path and that it is an NSF team uploaded algorithm  
            if($sqlStoredFP[0] == "C") { 
             $nativeFilePath .= $dbAlg_data['filePath'];
             $runner = ""; 
             //Make more dynamic later
             //J to indicae Java 
             $fileType = strpos($alg, ".py"); //expand to fit more languages later   
             if($fileType === false) {
               $nativeFilePath = chop($nativeFilePath, ".java"); 
               $runner = "java -classpath " .$nativeFilePath; 
             } else { 
               //if sensor id is none then we have web data and send in the filepath so the file data can be found 
               if($sensID == "none") {
                  $runner = "python3 " . $nativeFilePath . " " . $filePath; 
               } else {             
                  $runner = "python3 ".$nativeFilePath." ".$sensID." ".$sensor." ".$strmID;
               }
             }

              $command = escapeshellcmd($runner);   
              exec($command, $output, $rMessage);
              var_dump($output);
            } else {

            echo "$sensor $alg $sensID $strmID  " ;
	    $nonNativeFilePath .= $dbAlg_data['filePath'];
            //echo "File path executing is:  $fPath                  ";  
         
            $runner = ""; 
            //Make more dynamic later
            //J to indicae Java 
            echo "Alg data is : $alg";  

            $fileType = strpos($alg, ".py"); //expand to fit more languages later   
             if($fileType === false) {
              $nonNativeFilePath = chop($nonNativeFilePath, ".java");
              $javaTestArg = "/opt/stack/horizon/.blackhole/websockz/chat/ChatApp/trainingData"; 

              if($filePath != "none") {$javaTestArg = $filePath;} 
 
              $runner = "java -classpath " .$nonNativeFilePath . " " . $javaTestArg;  
            } else { 
              if($sensID == "none") {     
                  $runner = "python3 " . $nonNativeFilePath . " " . $filePath;
               } else {             
                  $runner = "python3 ".$nonNativeFilePath." ".$sensID." ".$sensor." ".$strmID;
               }

            }  
               
	    //$runner = "python3 ".$fPath." ".$sensID." ".$sensor." ".$strmID;
	    $command = escapeshellcmd($runner); 
            echo "<br>";
            echo "Runner is : $runner"; 
            //exec($command, $output, $rMessage); //testing for error with the Python 3 in the filepath bc it works with python fine 
            //var_dump($output);
            

            $message = exec("$command 2>&1"); // this way was producing useful error messages but I want output again
            echo "<br>"; 
            print_r($message); 
 

            //exec($command, $output, $rMessage); //testing for error with the Python 3 in the filepath bc it works with python fine 
            //var_dump($output);


            }
            //echo "$command"; 

            //For simple output        ****** Do it Like this for tests ****** [a, orb]
            //a good for java stuff
            //echo exec($command); 

            //exec($command, $output);
            //var_dump($output); 

            $time_end = round(microtime(true) * 1000); 
            $execution_time = ($time_end - $time_start);  
            //echo '<b>Total Execution Time:</b> '.$execution_time.' milliseconds';


            //I ran the $message and the exec section underneath it to get this error. 
            /*$output = array();
            $return_var = -1;
            $last_line = exec($command." "."2>&1", $output, $return_var);
    
            if ($return_var === 0) {
      // success
            }else{
      // fail or other exceptions
            throw new \Exception(implode("\n", $output));
    } */ 
  	
            

            /* $message = exec("$command 2>&1"); // this way was producing useful error messages but I want output again
             print_r($message); */ 
           /* exec($command, $out, $status);
             if (0 === $status) {
               var_dump($out);
               //print_r($out); 
             } else {
               echo "Command failed with status: $status";
               var_dump($out); 
               print_r($out); 
            } */ 
	    //echo $result;

	  }
          else
	    echo "hello";
          break;
        //====================== User login ======================//
        case 'login':
          include "login_backend.php";

          $response = user_login($_POST['login_email'],$_POST['login_pswd']);
          echo json_encode($response);
          break;
        //====================== User Registration ======================//
        case 'signUp':
          include "signUp_backend.php";

          $response = user_signUp($_POST);
          echo json_encode($response);
          break;
        //====================== Sign Out ======================//
        case 'signOut':
          include 'databaseFunctions.php';
          db_updateInfo('Accounts',['active'=>0],['email',$_SESSION['email']]);
          session_unset();
          $_SESSION['logged_in'] = false;
          echo 'success';
          break;

        //====================== File Upload ======================//
        case 'uploadFile':
          // Only logged in users can upload files
          if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']){
            include "upload_file.php";

            // Should be the name of the input that has the file
            $fileCmd  = 'uploadFile';
            $fileType = $_POST['fileType'];

            if($fileType == '0'){
              $fileType = "Algorithms";
              $fileCmd .= '-Alg';
            }
            elseif($fileType == '1'){
              $fileType = "Datasets";
              $fileCmd .= "-Data";
            }
            $fileTemp  = $_FILES[$fileCmd]['tmp_name'];
            $fileName  = $_FILES[$fileCmd]['name'];
            $fileSize  = $_FILES[$fileCmd]['size'];
            $userEmail = $_SESSION['email'];

            upload_file($fileType,$fileTemp,$fileName,$fileSize,$userEmail);
 	          header('location: ../account.php');
          }
          // User is not logged in
          else{
            echo "<script type='text/javascript'>alert('Access Required');</script>";
            header("location: ../index.html");
          }
          break;
        //====================== Command Not Known ======================//
        default:
          echo json_encode(['error'=>true,'errorMsg'=>'Command Not Found']);
      }
    }
    // No Command Given
    else{
      echo json_encode(['error'=>true,'errorMsg'=>'No Command Given']);
    }
  }

  //***************************** Handle GET Requests *****************************
  elseif($_SERVER["REQUEST_METHOD"] == "GET"){
    // Check if Command given
    if(!empty($_GET['cmd'])){

      switch($_GET['cmd']){

        //====================== Deletion of a Uploaded File ======================//
      	case 'removeFile':
      	  if(isset($_GET['filePath'])){
       	    $file = '/opt/stack/horizon/.blackhole/serverFiles/'.$_GET['filePath'];
      	    if(file_exists($file)){
      	      unlink($file);
      	    }
      	  }
       	  break;

        //====================== Simple Test Run ======================//
        case 'runTest':
          $command = escapeshellcmd('python /opt/stack/horizon/.blackhole/testFiles/statEx.py');
          $output  = shell_exec($command);
          echo $output;
          break;

        //====================== Get User's email id ======================//
        case 'loadID':
          // logged in user
          if(isset($_SESSION['email'])){
            echo json_encode(["email"=>$_SESSION['email']]);
          }
          // not logged in
          else{
            echo json_encode(['error'=>true,"email"=>"unknown","errorMsg"=>"email not set"]);
            $_SESSION['logged_in'] = false;
          }
          break;

        //====================== Command Not Known ======================//
        default:
          echo json_encode(['error'=>true,"errorMsg"=>"Command Not Found"]);
      }
    }
    // No Command Given
    else{
      echo json_encode(['error'=>true,'errorMsg'=>'No Command Given']);
    }
  }

 ?>
