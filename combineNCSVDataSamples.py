#Purpose: This file will be called when a request for realtime analysis has been made. It will combine N samples from
#******** the CSVs in the dataFiles directory. It will then (make modular?) save those files in the dataOutputs 
#******** directory in such a way that they will be uniquely identifiable between themselves, even if the files come  
#******** from the same sensorsID and streamID (probably a timestamp in the name to differentiate between them?


import csv 
import sys
import glob 
import itertools as IT
import pandas as pd
import numpy as np 
import os 
import shutil 


#need to pass in another argument ot help differentiate between diff samples of same sensorType and streamID
senseID = sys.argv[1]
senseType = sys.argv[2]
streamID = sys.argv[3]
timeStampDay = sys.argv[4] 
timeStampHMS = sys.argv[5]
sampleSize = sys.argv[6]

#quickly convert from string to int
sampleSize = int(sampleSize, 10) 
print "The arguments passed in are: " + senseID + senseType + streamID + timeStampDay + timeStampHMS + " endl python"


files = [] 

#store the filepaths to the x,y,z data of the relevant CSVs
#these will be read from reverse for 100 samples to do the N sample analysis 
#the result of this for loop  is the data "snapshot"  
for name in glob.glob('ChatApp/dataFiles/'+senseID+'*'+senseType+ '*' + streamID+'*'):
  files.append(name)

if not files:
  print "No data files with those specifications exist."
  sys.exit()
else:
  for f in files:
    directory = 'ChatApp/dataFiles/'+senseType+streamID+'/'
    if not os.path.exists(directory):
     os.mkdir(directory)
    shutil.copy(f, directory)

lens = [] 
maindata = [] 
count = 0
iterator = 0
#save the entire x,y,z file data in list maindata in reversed order  
for i in files:
  maindata.append(count)
  iterator = 0
  with open(i, "rb") as f:
    #reader = csv.reader(f) 
    data = [] 
    for row in reversed(list((csv.reader(f)))):
      if(iterator > 99) :
        maindata[count] = data
        count += 1
        break 
      data.append(float(row[0]))
      iterator += 1 


#read in 100 lines
#creates a file with 100 samples (where sample is a single row of x,y,z coordinates) that can be 
#accessed for analysis later  
def mkNSizedCombFile(npath):
  n = npath 
  if not os.path.exists(n):
    os.mkdir(n) 
  
  with open(n+senseID+'_'+senseType+'_'+streamID+'_'+timeStampDay+'_'+timeStampHMS+".csv", 'ab') as fd:
    for i in range(sampleSize): #read n samples 
      newline = [] 
      for c in range(count): 
        newline.append(np.float64(maindata[c][i]).item()) 
      writer = csv.writer(fd)
      writer.writerow(newline) 

n = 'ChatApp/dataOutputs/'+senseType+streamID+'out/'
mkNSizedCombFile(n)  	



