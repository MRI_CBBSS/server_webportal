<?php 
/*
Purpose: Uploads a single algorithm file to the database, or the "driver" file (aka the file with the main) into the database. We do not need to upload every file in a 
         algorithm into the database in the case of packages. This is because for simple packages, which is what we support at the moment, we only need to execute the 
         driver files when performing analysis. *This will be logged elsewhere, but, all analysis algorithms are executed for launching with a php exec command. All 
         algrithm files that need compilation will have that done in the upload process.  

*/ 



//This function will operate on the driver file of an upload. A key element here is that the user uploaded their algorithm name. We use that. 
//userEmail is likely not going to be an important to the tabe information but to the upload 
function uploadAlgToDb($algName, $signalType, $purpose, $ownerID, $filePath, $userEmail) {
//setup the database connection [really need to modularize this]
include "database.php"; 
$conn = mysqli_connect(DB_HOST,DB_USER, DB_PSWD, DB_NAME); 

//enter the variables
$email = $userEmail; 
$algName = $algName; 
$purpose = $purpose; 
$ownerID = $ownerID; 
$filePath = $filePath; 
if(!$conn) {
   die("Error: Could not connect to database"); 
} else {
  $query = "INSERT INTO Algorithms (algName, signalType, purpose, ownerID, filePath) VALUES ('".$algName."', '".$signalType."','".$purpose."','".$ownerID."','".$filePath."')"; 
  
  if(mysqli_query($conn, $query)) {
     echo "Records inserted successfully"; 
  } else {
     echo "ERROR: Could not execute $query. " .mysqli_error($conn); 
  }

}

$conn->close(); 
}






?> 
