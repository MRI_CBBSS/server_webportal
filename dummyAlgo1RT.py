import csv
import sys
import glob
import itertools as IT
import pandas as pd
import numpy as np
import os
import shutil
import time

#l = len(sys.argv) - 1
senseID = sys.argv[1] #the bluetooth address of the sensor being observed
senseType = sys.argv[2] #accelerometer, gyroscope, etc.
streamID = sys.argv[3] #indicates the group of sensors a sensor belongs to
timestamp = sys.argv[4] #indicates the time the csv was created. Used to find file in dataOutputs 

timeStampList = timeStamp.split() #splits into date and Hours:minutes:seconds 

filePathToNSamples =  glob.glob('/opt/stack/horizon/.blackhole/websockz/chat/ChatApp/dataOutputs/'senseType+streamID+'out/'+senseID+'_'+senseType+'_'+streamID+'_'+timeStampList[0]+'_'+timeStampList[1]+'.csv') 
 

lens = []
maindata = []
count = 0


with open(filePathToNSamples, "rb") as f:
  reader = csv.reader(f)
    for row in reader:
      maindata.append(float(row[0]))

avg = sum(maindata)/len(maindata)
print ("Average of 100 previous samples: "+str(avg))
print ("This is the data of the 100 previous samples: " + str( maindata))

