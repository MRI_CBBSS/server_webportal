import csv
import sys
import glob
import itertools as IT
import pandas as pd
import numpy as np
import os

#l = len(sys.argv) - 1
senseID = sys.argv[1]
senseType = sys.argv[2]	#accelerometer, gyroscope, etc.
streamID = sys.argv[3]

fs = []

for name in glob.glob('ChatApp/dataFiles/'+senseID+'*'+senseType+'*'+streamID+'*'):
  fs.append(name)
#  print(name)

lens = []
maindata = []
count = 0

for i in fs:
  maindata.append(count)
  with open(i, "rb") as f:
    reader = csv.reader(f)
    d = reader.next()
    data = pd.read_csv(i)
    j = data[d[0]].values
    maindata[count] = j
    count += 1

with open("ChatApp/dataOutputs/"+senseID+'_'+senseType+'_'+streamID+".csv", 'ab') as fd:
  for i in range(len(maindata[0])):
    newline = []
    for c in range(count):
      newline.append(np.float64(maindata[c][i]).item())
    print newline
    writer = csv.writer(fd)
    writer.writerow(newline)
