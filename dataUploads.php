<?php 

/*
  Purpose: This allows a user to upload their data files to the server's database and the server's filesystem.
  How it works: Grab the user's file information from the $_FILES variables and store them in new variables. Pass these
                variables in as parameters to the upload_data_files function, where it will do all the necessary file handling to ensure the upload occurs.
                A successful upload occurs when the user has heir file in the filesystem and the database's Datasets section. 
*/


session_start(); 




if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']){

//ensure the $_FILES[] global array is set
           if (isset($_FILES['data'])) {

             //Grab the necessary information for the file upload process
             $file_name_array = $_FILES['data']['name'];
             $tmp_name_array = $_FILES['data']['tmp_name'];
             $type_array = $_FILES['data']['type'];
             $size_array = $_FILES['data']['size'];
             $error_array = $_FILES['data']['error'];
             $fileType = "Datasets";    //may remove. File type for algorithms that is used in the upload_file function to determine directory files will be uploaded to
             $userEmail = $_SESSION['email'];
             $pathToJavaFilesArray = [];

             $sensor_name = $_POST['sensor_name'];              


             for($i=0; $i < count($tmp_name_array); $i++) {
               echo $tmp_name_array[$i] . " " . $file_name_array[$i] . " " . $size_array[$i];
               echo "<br>";
             }
            }
            include 'upload_data_files.php';  
            upload_data_files($fileType, $tmp_name_array, $file_name_array, $size_array, $userEmail, 
                                $sensor_name); 
}
?>
