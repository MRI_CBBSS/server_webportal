<?php 

/*
Purpose: This script will allow a user to download an algorithm or data file from the server. It receives information stored in the web portal's 
         data and algorithm tables and uses that to perform a query for the correct filepath. Once it has the filepath of the file, it is downloaded 
         and sent to the user for them to download onto their computer. 

*/
session_start(); 

if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']) { 

  $fileName =  $_GET["fileName"]; 
  $userID   =   $_GET["email"]; 
  $sensorType = $_GET["typeGen"]; 
  $dataSetSource = $_GET["source"]; 

  //Connect to the database to perform a query. The goal of the query is to retrieve the filepath 
  //of the dataset that the user wishes to download. THis is necessary for the read_file php function
  $conn = new mysqli('localhost', 'matt', 'CSci@5657', 'csufml_db');

  if($conn->connect_error) {
    die("Error: Could not connect to database". $conn->connect_error); 
  } 


  $query = "SELECT path from Datasets WHERE 
            userId = '".$userID."' AND   
            fileName = '".$fileName."' AND 
            typeGen  = '".$sensorType."' AND 
            source   = '".$dataSetSource."'";

  $result = $conn->query($query); 
 
  //TODO: Add check to ensure we do not get more than one row
  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $targetFilePath = $row["path"]; 
     }
   } else {
     // echo "0 results";
  }

  //now query stores the filepath of the file we want to read
  //ensure the file exists before attempting to download it 
  if(file_exists($targetFilePath)) {
    //File exists so begin download code 
     header('Content-Type: application/octet-stream');
     header("Content-Disposition: attachment; filename=\"".basename($targetFilePath)."\"");
     readfile($targetFilePath);
     exit();

  } else {
    echo "file does not exist will not be downloaded"; 
    die("Failure: File does not exist"); 
  }

  $conn->close();     
}
 
?>
~ 
