import glob
import errno
import os
import random
import sys
import numpy as np 
import pandas as pd
#print("Before SKlearn") 
from sklearn.naive_bayes import GaussianNB
#print("post import") 

#collect sensor information
senseID = sys.argv[1] #bluetooth sensor
senseType = sys.argv[2] #sensor type 
streamID = sys.argv[3] #stream group
timeStampDay = sys.argv[4] #timestamp of newly created 100 samples 
timeStampHMS = sys.argv[5] #timestamp of newly created 100 samples 


CSV_PATH=glob.glob('/opt/stack/horizon/.blackhole/websockz/chat/ChatApp/trainingData/')

#print(CSV_PATH)
#path = CSV_PATH + "/trainingData/*.csv" # for direct file
#print(path)


# In[5]:


def create_segments(batch_size, data): #batch_size is the segment size
    row=data.shape[0]
    col=data.shape[1]
    #print(row)
    
    if row%batch_size ==0:
        num_batches=int(row/batch_size)
    else:
        num_batches=int(row//batch_size)
        num_batches=num_batches+1
    
    st=np.array([0],dtype=int)
    en=np.array([batch_size-1],dtype=int)
    for i in range(1, num_batches):
        last_en=en[i-1]
        st=np.append(st,last_en+1)
        en =np.append(en,last_en+batch_size)
        
    if(en[-1]>row):
        en[-1]=row
        
    return st,en, num_batches

def random_int():
   return random.randint(1,10)

#In[6]:


#print("Before adding files to file array")
files = []
for name in glob.glob('/opt/stack/horizon/.blackhole/websockz/chat/ChatApp/trainingData/'+'*.csv'):
  files.append(name)
 # print(name)



batch_size=50

features=np.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
#print("After features np array defined in train module") 
#print(features.shape)
for name in files:
    try:
        with open(name) as f:
            #print ("This is name: " + name)
            #print(len(name))
            #print("Before pandas first module") 
            test_data = pd.read_csv(name, header=None) # do what you want
            #print("After pandas first module") 
            #print(test_data.head(10)) 
            
            #print("Before create segemnts first module")
            st,en, num_batches=create_segments(batch_size, test_data)
            #print("after create segments first module") 
            if 'a'==name[65]:
                target=0
            else:
                target=1
            
            
                      
            #print("Test data values only")
            #print (test)
            data = test_data.values   
            for i in range(1, st.shape[0]):
                seg=data[st[i]:en[i],1:]
               # print("seg is this") 
               # print (seg)
                row, col=seg.shape
                #print(col)
                
                ef=np.array([np.percentile(seg[:,0],0),np.percentile(seg[:,0],25),np.percentile(seg[:,0],50),np.percentile(seg[:,0],75),np.percentile(seg[:,0],100),target])
                    
                #feat=np.array([])
                for j in range(1,col):
                    feat=np.array([np.percentile(seg[:,j],0),np.percentile(seg[:,j],25),np.percentile(seg[:,j],50),np.percentile(seg[:,j],75),np.percentile(seg[:,j],100),target])
                    ef=np.hstack((ef,feat))
                
                #print(ef.shape)
                features=np.vstack((features, ef)) 
                
    except IOError as exc:
        if exc.errno != errno.EISDIR:
            raise


# In[7]


#print(features.shape)
features=np.delete(features, 0, 0)
#print(features.shape)
#print(features)
 

#In[8]:


# Do the same for test data
#CSV_PATH=glob.glob('/opt/stack/horizon/.blackhole/websockz/chat/ChatApp')

#print(CSV_PATH)
#path=CSV_PATH +"/dataOutputs/"+senseType+streamID+"out/"+senseID+"_"+senseType+"_"+streamID+ ".csv"
#path = CSV_PATH + "/dataOutputs/Gyroscope1out/00:06:66:88:D9:37_Gyroscope_1.csv" # for direct file
#print("Inside the test section now") 
#print(path)


 
#print("Before adding files to file test array")

filess = []
for name in glob.glob('/opt/stack/horizon/.blackhole/websockz/chat/ChatApp/dataOutputs/' + senseType + streamID + 'out/' + senseID + '_' +senseType+ '_' + streamID +  '_' +  timeStampDay + '_' +  timeStampHMS +  '.csv'):
  filess.append(name)
  #print(name)



batch_size=50

Tfeatures=np.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
#print(Tfeatures.shape)
for name in filess:
    try:
        with open(name) as f:
            test_data = pd.read_csv(name, header=None) # pandas read_csv header=None option for when you data csvs do not have a header or not all data will be read
            #print(test_data) 
            #print(test_data.head(10))
            test = np.array(test_data).tolist()

            #will insert values into this array. Good padding data comes second. First is adding the correct amount of data dynamically.
            #1st get row count. 2nd add 3 columns of data to every array. 3st add real padding data not just 0s
            row_count = len(test)
            #print("Row count is: " + str(row_count))
            for row in test:
              for i in range(0,2):
                 row.insert(0,1)
            test = np.array(test)
            #print("The new array before segmentation")
            #print(test)
            st,en, num_batches=create_segments(batch_size, test)
            
            if 'a'==name[28]:
                target=0
            else:
                target=1

            for i in range(1, st.shape[0]):
                seg=test[st[i]:en[i],1:]
                row, col=seg.shape
                #print(col)
                
                ef=np.array([np.percentile(seg[:,0],0),np.percentile(seg[:,0],25),np.percentile(seg[:,0],50),np.percentile(seg[:,0],75),np.percentile(seg[:,0],100),target])
                    
                #feat=np.array([])
                for j in range(1,col):
                    feat=np.array([np.percentile(seg[:,j],0),np.percentile(seg[:,j],25),np.percentile(seg[:,j],50),np.percentile(seg[:,j],75),np.percentile(seg[:,j],100),target])
                    ef=np.hstack((ef,feat))
                
                #print(ef.shape)
                Tfeatures=np.vstack((Tfeatures, ef)) 
                
    except IOError as exc:
        if exc.errno != errno.EISDIR:
            raise
            
            
#print(Tfeatures.shape)
Tfeatures=np.delete(Tfeatures, 0, 0)
#print(Tfeatures.shape)
#print(Tfeatures)


# In[9]:


trainX=features[:,0:22]
trainY=features[:,23]

testX=Tfeatures[:,0:22]
testY=Tfeatures[:,23]


gnb = GaussianNB()


# In[10]:


y_pred = gnb.fit(trainX, trainY).predict(testX)
#print("Number of mislabeled points out of a total %d points : %d" % (testX.shape[0],(testY != y_pred).sum()))

#for i in range(len(testX)):
	#print("X=%s, Predicted=%s" % (testX[i], y_pred[i]))
   
#print ("The predictions for the samples: " + y_pred) 

fall = False
for i in y_pred:
   if i == 1:
     fall = True

if fall == True :
   print("Fall  detected")
else:
   print("No fall detected")         



