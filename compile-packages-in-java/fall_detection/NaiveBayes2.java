package fall_detection;

import java.util.ArrayList;

public class NaiveBayes2 {
	
	private ArrayList<ArrayList<Double>> trainX;
	private ArrayList<ArrayList<Double>> testX;
	
	private ArrayList<Integer> trainY;
	private ArrayList<Integer> testY;
	
	private ArrayList<Integer> numBins;
	
	private ArrayList<ArrayList<Integer>> disFeat;
	private Integer countYes;
	private Integer countNo;
	private Integer norm=3;
	private Double prior=0.5;
	
	
	private ArrayList<ArrayList<Integer>> trainStructureYes =new ArrayList<ArrayList<Integer>>();
	private ArrayList<ArrayList<Integer>> trainStructureNo =new ArrayList<ArrayList<Integer>>();
	
	public NaiveBayes2(ArrayList<ArrayList<Double>> trainX, 
			ArrayList<Integer> trainY, ArrayList<ArrayList<Double>> testX, 
			ArrayList<Integer> testY, ArrayList<Integer> numBins, Integer countYes, Integer countNo){
		
		this.trainX=trainX;
		this.trainY=trainY;
		this.testX=testX;
		this.testY=testY;
		this.numBins = numBins;
		System.out.println("Number of bins is: " + numBins);
		disFeat= getDis(trainX); //feature distribution of trainX data 
		System.out.println("Feature distribution is: " + disFeat);
		this.countYes=countYes;
		this.countNo=countNo;
		train();
		
		
		ArrayList<ArrayList<Integer>> testX_discrete= getDis(testX);
		//System.out.println(testX_discrete.size());
		
		ArrayList<ArrayList<Integer>> testX_dis= new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> fg=new ArrayList<Integer>();
		int testsize2= testX_discrete.get(0).size();
		int testsize3= testX_discrete.size();
		System.out.println(testsize3);
		System.out.println(testsize2);
		
		for(int i=0; i<testsize2;i++){
			for(int j=0; j<testsize3;j++){
				fg=new ArrayList<Integer>();
				fg.add(testX_discrete.get(j).get(i));
			}
			
			testX_dis.add(fg);
		}
		
		int testsize= testX_dis.size();
		ArrayList<Integer> predictions=new ArrayList<Integer>();
		for(int i=0; i<testsize; i++){
			ArrayList<Integer> test1=testX_dis.get(i);
			int label = predictOne(test1);
			predictions.add(label);
		}
		
		double g=accuracy(predictions,testY);
		System.out.println("This is the accuracy " + g);
		
	}
	
	//pass in trainX data. Aka, features of the training data. Get a histogram that represents the feature distribution
	private ArrayList<ArrayList<Integer>> getDis(ArrayList<ArrayList<Double>> tt){
		Histogram hist=new Histogram(tt, numBins);
		ArrayList<ArrayList<Integer>> disFeat= hist.getDis();
		return disFeat;
	}
	
	private void train(){
		for(int i=0; i<numBins.size(); i++){
			Integer a = numBins.get(i);
			System.out.println("Number of bins in train is: " + numBins);
			System.out.println("Value of bins in train is: " + a);
			ArrayList<Integer> featureValueCountYes=new ArrayList<Integer>(a);
			ArrayList<Integer> featureValueCountNo=new ArrayList<Integer>(a);
			//System.out.println(featureValueCountYes.size());
			
			for(int k=0; k< a; k++){
				//System.out.println(k);
				featureValueCountYes.add(0);
				
				featureValueCountNo.add(0);
			}
			System.out.println("Feature value count for yes" + featureValueCountYes);
			System.out.println("Feature value count for no" + featureValueCountNo);
			ArrayList<Integer> binId=new ArrayList<Integer>(a);
			for(int j=0; j<a;j++){
				binId.add(j);
			}
			
			
			ArrayList<Integer> df=disFeat.get(i);
			
			for(int j=0; j<df.size(); j++){
				
				if(trainY.get(j)==1){
					
					for(int k=0; k<a; k++){
						if(df.get(j)==binId.get(k)){
							featureValueCountYes.set(k, featureValueCountYes.get(k)+1);
						}
					}
				}
				else{
					for(int k=0; k<a; k++){
						if(df.get(j)==binId.get(k)){
							featureValueCountNo.set(k, featureValueCountNo.get(k)+1);
						}
					}
					
				}
			}
			
			trainStructureYes.add(featureValueCountYes);
			trainStructureNo.add(featureValueCountNo);
			
		}
		System.out.println("###############");
		System.out.println(trainStructureYes);
		System.out.println("###############");
		System.out.println(trainStructureNo);
	}
	
	private Integer predictOne(ArrayList<Integer> test1){
		int siz=test1.size();
		//System.out.println(siz);
		
		
		double prob_fgivenYes=1;
		double prob_fgivenNo=1;
		for (int i=0;i<siz; i++){
			int f_val=test1.get(i);
			prob_fgivenYes = prob_fgivenYes*(trainStructureYes.get(i).get(f_val)+ norm*prior)/(countYes+ norm);
			prob_fgivenNo = prob_fgivenNo*(trainStructureNo.get(i).get(f_val)+ norm*prior)/(countNo+ norm);
		}
		prob_fgivenYes=prob_fgivenYes*(countYes/(countYes+countNo));
		prob_fgivenNo=prob_fgivenNo*(countNo/(countYes+countNo));
		
		Integer label;
		if(prob_fgivenYes>prob_fgivenNo){
			label=1;
		}
		else{
			label=0;
		}
		
		return label;
		
	}
	
	private double accuracy(ArrayList<Integer> a, ArrayList<Integer> b){
		int siz=a.size();
		int acc=0;
		
		for(int i=0; i<siz; i++){
			if(a.get(i)==b.get(i)){
				acc=acc+1;
			}
		}
		return acc/siz;
		
	}

}
