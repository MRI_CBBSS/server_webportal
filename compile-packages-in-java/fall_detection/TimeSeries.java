package fall_detection;

public class TimeSeries {
	
	private Double[] data;
	private Double[] time;
	private Integer length;
	
	public TimeSeries(Double[] x){
		data=x;
		//time=t;
		length= (int) data.length;
	}
	
	public Double[] getData(){
		return data;
	}
	
	public Double[] getTime(){
		return time;
	}
	
	public void setTime(Double[] t){
		time=t;
	}
	
	public Integer getLength(){
		return length;
	}

}
