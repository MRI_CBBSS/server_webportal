package fall_detection;


import java.util.ArrayList; 
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CreateTrainTest {
	
	private static final Double PERCENT_TRAIN=80.0;
	private static final Double PERCENT_TEST=100.0-80.0;
	
	private ArrayList<ArrayList<Double>> trainX=new ArrayList<ArrayList<Double>>();
	private ArrayList<ArrayList<Double>> testX=new ArrayList<ArrayList<Double>>();
	
	private ArrayList<Integer> trainY=new ArrayList<Integer>();
	private ArrayList<Integer> testY=new ArrayList<Integer>();
	
	private Integer countYes=0;
	private Integer countNo=0;
	
	private double NUM_SAM=50.0;
	
	//create a set of training and test data by passing in the fall and not fall arrays, segmenting their data to make them more useful, and then grabbing their mean, standard deviation, and max.
	//This creates a set of synthetic test data using the training data.
	public CreateTrainTest(ArrayList<ArrayList<Double>> fall, ArrayList<ArrayList<Double>> notfall){
		//ArrayList<Integer> fallY = getY(fall, true);
		//ArrayList<Integer> notfallY = getY(notfall, false);
		
		ArrayList<ArrayList<Double>> fallF=getMeanVarFeatures(fall);
		ArrayList<ArrayList<Double>> notfallF=getMeanVarFeatures(notfall);
		
		int fsize=fallF.size();
		System.out.println("The unsegmented size of the fall array: " + fall.size());
		System.out.println("The size of features array for fall:" + fsize);
		int nfsize=notfallF.size();
		System.out.println("The size of features array for notFall:" + nfsize);
		
		/////////////////////////////////////////////////
		List<Integer> listFall = new ArrayList<Integer>();
		for (int i=0;i<fsize+nfsize;i++){
			listFall.add(i);
		}
		//System.out.println(listFall.toString());
		java.util.Collections.shuffle(listFall);
		////////////////////////////////////////////////
		
		int num_train= (int) Math.ceil((fsize+nfsize)*(PERCENT_TRAIN/100));
		//System.out.println(fsize);
		//System.out.println(nfsize);
		//System.out.println(nfsize+fsize);
		//System.out.println(num_train);
		
		
		/*Integer count=0;
		for(int i=0;i<fsize;i++){
			ArrayList<Double> a=fallF.get(i);
			System.out.println(a.size());
			count=count+1;

		}
		System.out.println(count);
		System.out.println(nfsize);*/
		
		
		/////////////////////////////////////////////
		for (int i=0;i<num_train;i++){
			int index = listFall.get(i);
			if(index<fsize){ //if our index value exists within the bounds of the fallFeatures list, then it is a fall
				trainX.add(fallF.get(index));
				//System.out.println("ahaaa!");
				trainY.add(1);
				countYes=countYes+1;
			}
			else{
				trainX.add(notfallF.get(index-fsize));
				trainY.add(0);
				countNo=countNo+1;
			}
			//System.out.println(trainY.get(i));
		}
		System.out.println(trainX.size());
		
		///////////////////////////////////////////////
		
		//I think the data we have needs to be put here. Buut, this data is heavily affected by the test data segmentation and features
		for (int i=num_train+1;i<fsize+nfsize;i++){
			int index = listFall.get(i);
			if(index<fsize){
				System.out.println("fefer"+index);
				testX.add(fallF.get(index));
				testY.add(1);
			}
			else{
				testX.add(notfallF.get(index-fsize));
				testY.add(0);
			}
		}
		System.out.println(testX.size());
	
		
	}
	
	public Integer getCountYes(){
		return countYes;
	}
	
	public Integer getCountNo(){
		return countNo;
	}

	public ArrayList<ArrayList<Double>> getTrainX(){
		return trainX;
	}
	
	public ArrayList<ArrayList<Double>> getTestX(){
		return testX;
	}
	
	public ArrayList<Integer> getTestY(){
		return testY;
	}
	
	public ArrayList<Integer> getTrainY(){
		return trainY;
	}
	
	//grabbing mean var features of the fall and notFall vectors
	//we grab the mean, standard deviation, and the max for every vector of 2nd column values in the fall and notFall vectors after they have been segmented. 
	//We return a list of lists of features for each of the segmented fall and notFall arrays. 
	private  ArrayList<ArrayList<Double>> getMeanVarFeatures(ArrayList<ArrayList<Double>> arr){
		ArrayList<ArrayList<Double>> seg_all=getSegments(arr);
		System.out.println("The size of one of the segmented arrays is: " + seg_all.size());
		
		ArrayList<ArrayList<Double>> features=new ArrayList<ArrayList<Double>>();
		
		
			
		for (ArrayList<Double> a : seg_all){
			ArrayList<Double> feat=new ArrayList<Double>(3);
			double sum=0;
			int n=a.size();
			
			
			for (int i=0;i<n;i++){
				sum=sum+a.get(i);
			}
			double mean=sum/n;
			
			double sum2=0;
			
			for (int i=0;i<n;i++){
				sum2=sum2+ (a.get(i)-mean)*(a.get(i)-mean);
			}
			
			double std = Math.sqrt(sum2/(n-1));
			//System.out.println(std);
			double max=a.get(0);
			for (int i=1;i<n;i++){
				if(max<a.get(i)){
					max=a.get(i);
				}
			}
			
			feat.add(mean);
			feat.add(std);
			feat.add(max);
			feat.trimToSize();
			//System.out.println(feat.size());
			
			features.add(feat);
			
    	}
		//System.out.println(features.get(1).size());
		
		return features;
		
	}
	
	//called on fall and notFall vectors. It segments the data to make it more meaningful for further analysis 
	private ArrayList<ArrayList<Double>> getSegments(ArrayList<ArrayList<Double>> arr){
		ArrayList<Double> seg_temp=new ArrayList<Double>();
		ArrayList<ArrayList<Double>> seg_all=new ArrayList<ArrayList<Double>>();
		
		for (ArrayList<Double> a : arr){
			Integer siz=a.size();
			//System.out.println("In getSegments. Size of each array is :" + siz);
			//System.out.println(getStringInt(a));
			int num_seg= (int) Math.floor(siz/NUM_SAM)+1;
			
			int st=0;
			int en=(int) (NUM_SAM);
			for (int i=0; i<num_seg; i++){
				//System.out.println(en);

				seg_temp=subList(st, en, a);
				seg_temp.trimToSize();
				if(seg_temp.size()>0){
					seg_all.add(seg_temp);
				}
				st=en+1;
				en=en+(int) (NUM_SAM)+1;
				if(en>siz){
					en=siz;
				}
							}	
			
    	}
		seg_all.trimToSize();
		return seg_all;
	}
	
	private ArrayList<Double> subList(int st,int en,ArrayList<Double> array){
		ArrayList<Double> result=new ArrayList<Double>();
		for (int i=st; i<en; i++){
			result.add(array.get(i));
		}
		result.trimToSize();
		return result;
	}
	
	private ArrayList<Integer> getY(ArrayList<ArrayList<Double>> arr, boolean fall){
		ArrayList<Integer> Y=new ArrayList<Integer>();
		
	    if(fall){
	    	for (ArrayList<Double> a : arr){
	    		Y.add(1);}
	    	}
		else{
			for (ArrayList<Double> a : arr){
	    		Y.add(0);}
		}
		return Y;
	}
	
	
	public static StringBuilder getStringInt(ArrayList<Double> row){
		
		StringBuilder sb =new StringBuilder();
		for (Double d: row){
			String s=d.toString();
			sb.append(s);
			sb.append("\t");
		}
		return sb;		
	}
	
	
	
	
	

}
