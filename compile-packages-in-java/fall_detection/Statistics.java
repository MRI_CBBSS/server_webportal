package fall_detection;

import java.util.Arrays;

public final class Statistics{
	
	public static Double Mean(TimeSeries ts){
		Double[] data=ts.getData();
		Integer size = ts.getLength();
	
		
		Double sum = 0.0;
        for(int i=0; i<=size-1; i++){
            sum = sum +data[i];
        }
        Double m = sum/size;
        
		return m;
	}
	
	public static Double Variance(TimeSeries ts){
		Double[] data=ts.getData();
		Integer size = ts.getLength();
		
		Double mean =Mean(ts);
		Double sumTemp =0.0;
		
		for(int i=0; i<=size-1; i++){
			sumTemp=sumTemp+ ((data[i]-mean)*(data[i]-mean));
		}
		
		Double var =sumTemp/size;
		return var;
	}
	

	public static Double Std(TimeSeries ts){
		return Math.sqrt(Variance(ts));
	}
	
	 public static Double Median(TimeSeries ts){
		 Double[] data=ts.getData();
		 Integer size = ts.getLength();
		 
	      Arrays.sort(data);

	       if (data.length % 2 == 0){
	          return (double) (data[(size/2) - 1] + data[(size/2)]) / 2.0;
	       } 
	       
	       return data[(int) Math.floor(size/2)];
	    }
	 
	 public static Double Quantiles(TimeSeries ts, Double dp){
		 Double[] data=ts.getData();
		 Integer size = ts.getLength();
		 
	     Arrays.sort(data);
	     Double[] quantiles=new Double[size];
	     
	     int index=-1;
	     Double q=0.0;
	     
	     for(int i=0; i<=size-1; i++){
				quantiles[i]=(i+1-0.5)/size;				
					if(i>0){						
						if(dp>= quantiles[i-1] && dp<=quantiles[i]){
							index=i;						
							break;
						}
					}
	     }
	

	     if(index !=-1){
	    	 
			if(index<=size-1){
				q= data[index-1] + (data[index]-data[index-1])*((dp-quantiles[index-1])/(quantiles[index]-quantiles[index-1]));
			}
			
	     }
	     else if((1-dp) <=(dp-0)){
	    	 q=data[size-1];
	     }
	     else{
	    	 q=data[0];
	     }
	     
	      
	     return q;
	 }
	 
	 public static Double IQR(TimeSeries ts){
		 return Quantiles( ts, .75)-Quantiles( ts, .25);
	 }
	 
	 public static Double Max(TimeSeries ts){
		 return Quantiles( ts, 1.0);
	 }
	 
	 public static Double Min(TimeSeries ts){
		 return Quantiles( ts, 0.0);
	 }
}
