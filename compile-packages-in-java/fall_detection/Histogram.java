package fall_detection;

import java.util.ArrayList;
//import SensorSide.Statistics; //used to be SensorSide.Statistics 
//import SensorSide.TimeSeries; //used to be SensorSide.TimeSeries 

public class Histogram {
	
	private Integer numFeat;
	private ArrayList<ArrayList<Integer>> verFeatDis;
	
    public Histogram(ArrayList<ArrayList<Double>> features,ArrayList<Integer>  numBins) {
    	ArrayList<Double> feat=features.get(0);
    	numFeat=feat.size();
        verFeatDis =discretization( features, numBins);
        
    }
    
    public ArrayList<ArrayList<Integer>> getDis(){
    	return verFeatDis;
    }
    
    private ArrayList<ArrayList<Double>> getFeatureColumns(ArrayList<ArrayList<Double>> features){
    	ArrayList<ArrayList<Double>> verticalFeatures = new ArrayList<ArrayList<Double>>(numFeat);
    	 
    	for(int i=0; i<numFeat; i++){
    		ArrayList<Double> f123= new  ArrayList<Double>();
    		
    		for( ArrayList<Double> f: features){
    			f123.add(f.get(i));
    		}
    		
    		verticalFeatures.add(f123);
    	}
    	return verticalFeatures;
    }
    
    private ArrayList<ArrayList<Integer>> discretization(ArrayList<ArrayList<Double>> features, ArrayList<Integer> numBins){
    	ArrayList<ArrayList<Double>> verticalFeatures= getFeatureColumns(features);
    	ArrayList<ArrayList<Integer>> verticalFeaturesDis= new ArrayList<ArrayList<Integer>>(numFeat);
    	
    	for(int i=0; i<numFeat; i++){
    		ArrayList<Double> f=verticalFeatures.get(i);
    		ArrayList<Integer> dis_f=new ArrayList<Integer>();
    		
    		TimeSeries ts= new TimeSeries(f.toArray(new Double[f.size()]));
    		Double maxVal=Statistics.Max(ts);
    		Double minVal=Statistics.Min(ts);
    		Double window_size = (maxVal-minVal)/numBins.get(i);
    		
    		Double[] cutoff_begin = new Double[numBins.get(i)];
    		Double[] cutoff_end =new Double[numBins.get(i)];
    		Double st=minVal;
    		
    		for(int j=0; j<numBins.get(i); j++){
    			cutoff_begin[j]=st;
    			cutoff_end[j]=st+window_size;
    			st=st+window_size+0.00001;
    			
    			if(j==numBins.get(i)-1){
    				System.out.println(maxVal.toString() + "___" + cutoff_end[j].toString());
    			}
    			
    			for(Double d: f){
        			if(d>=cutoff_begin[j] && d<cutoff_end[j]){
        				dis_f.add(j);
        			}
        		}
    			
    			
			}
    		
    		verticalFeaturesDis.add(dis_f);
    		//System.out.println(verticalFeaturesDis.toString());
    		
    		
    	}
    	
    	return verticalFeaturesDis;
    }
    
    private void createHistogram(ArrayList<ArrayList<Double>> features){
    	//ArrayList<ArrayList<Double>> verticalFeatures= getFeatureColumns(features);
    }

    
}

