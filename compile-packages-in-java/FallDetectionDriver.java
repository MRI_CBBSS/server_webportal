import fall_detection.NaiveBayes2;
import fall_detection.Statistics;
import fall_detection.TimeSeries;
import fall_detection.Histogram;
import fall_detection.CreateTrainTest;


import java.io.BufferedReader; 
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
 

public class FallDetectionDriver {
	
	public static StringBuilder getStringInt(ArrayList<Double> row){
		
		StringBuilder sb =new StringBuilder();
		for (Double d: row){
			String s=d.toString();
			sb.append(s);
			sb.append("\t");
		}
		return sb;		
	}

	public static void main(String[] args){
		
		//get the current working directory and create the folderpath
		//String path =System.getProperty("user.dir");
		//String folderpath =path + "\\" + "data";
                String folderpath = "/opt/stack/horizon/.blackhole/websockz/chat/ChatApp/trainingData"; 
		System.out.println("Training Data is at: " + folderpath);
	        	
		File folder = new File(folderpath);
		File[] listOffiles= folder.listFiles();	 

		ArrayList<ArrayList<Double>> fall=new ArrayList<ArrayList<Double>>();
                ArrayList<ArrayList<Double>> notfall=new ArrayList<ArrayList<Double>>();
	
		for (File file: listOffiles){
			String csvfile=file.getName();
			String line = "";
	                String cvsSplitBy = ",";

	        String category =csvfile.split("")[0];
	        //TODO: Where does a and f come from? What do they mean in this context? We undertand they match with the first letter of the file name, but what are the expected file names?
	        //TODO: Can you send us an example folder of the data files.This will help us undertsand the format that is required to run this driver program. 
	        //TODO: Which sets of data are the files that we are reading in going to be using? Accelerometer, gyroscope, etc
	        if(category.equals("a") || category.equals("f")){	        
		        try (BufferedReader br = new BufferedReader(new FileReader(folderpath+"/"+csvfile))) {
	        
		        	
		        	ArrayList<Double> temp=new ArrayList<Double>();
		        	while ((line = br.readLine()) != null) {
		        		//System.out.println("The csv file line by line is:" + line);  		
	            		String[] accVals = line.split(cvsSplitBy); //array gets filled with column values from csv
	            		Integer length=accVals.length;
	            		//System.out.println(accVals.length);
	            		Double val= Double.parseDouble(accVals[1]); //TODO: why is it grabbing the second value?  //old: new double(accVals[1]);
	        			temp.add(val);
	            		
		        	}
		        	
		        	temp.trimToSize(); //array filled with the second column item of the csv file...why second?
		        	//System.out.println("The array after the loop is: " + temp); 
		        	
		        	
		        	//System.out.println("Splitting csvfile again result in: " + csvfile.split("")[0]);
		        	//fill the fall or notfall vector of vector's with type double (which here is our temp vector, aka, all of the 2nd column values of the entire csv) 
		        	//when we match "a" or "f" filenames 
		        	//TODO: Decide if this implementation needs to be changed. Like if we don't know if it is a fall yet, how can we call it one? However, this might be the set of training data.
		            if(category.equals("a")){
		            	notfall.add(temp);
		            }
		            else{
		            	fall.add(temp);
		            	//System.out.println("dfgkdgkjdfngjn");
		            }
		            //System.out.println(getStringInt(temp));
		        	
		        } catch (FileNotFoundException e) {		
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
		        
			}
			//System.out.println(System.getProperty("user.dir"));					
		}
		//Integer NrowCol =notfall.size();
		//System.out.println(NrowCol.toString());
		
		//after for loop
		//TODO: Understand what test and train do in context of Naive Bayes algorithm::::So, trainX is a set of data that tracks the data for a confirmed fall or not fall. trainY tracks if the data represents a fall or not. a trainY of 0 indicates notFall, while 1 indicates fall. The trainX and trainY are the same size
		CreateTrainTest ctt = new CreateTrainTest(fall, notfall);
		ArrayList<ArrayList<Double>> trainX=ctt.getTrainX();
		ArrayList<ArrayList<Double>> testX=ctt.getTestX();
		
		ArrayList<Integer> trainY=ctt.getTrainY();
		ArrayList<Integer> testY=ctt.getTestY();
		
		int count=0;
		for (ArrayList<Double> a : trainX){
			System.out.println(a.toString() + " " + trainY.get(count).toString() );
			count=count+1;
		}
		
		/////// Implement Naive Bayes
		//TODO: Are we going to be writing the output, or is there something your team has in mind for handling that?
		ArrayList<Integer> numBins = new ArrayList<Integer>(3);
		numBins.add(2);
		numBins.add(2);
		numBins.add(2);
		NaiveBayes2 nb=new NaiveBayes2(trainX,trainY, testX, testY, numBins,ctt.getCountYes(),ctt.getCountNo());
		/*ArrayList<ArrayList<Integer>> disFeat= hist.getDis();
		System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		System.out.println(disFeat.size());*/
			
	}

}
