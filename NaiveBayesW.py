import glob
import errno
import os
import random
import sys
import numpy as np 
import pandas as pd
#print("Before SKlearn") 
from sklearn.naive_bayes import GaussianNB
#print("post import") 

#path to the testing data that will be passed into the algorithm for analysis
testingDataPath = sys.argv[1] #bluetooth sensor

#path to the training data that is at this point the default for the system  
CSV_PATH=glob.glob('/opt/stack/horizon/.blackhole/websockz/chat/ChatApp/trainingData/')

def create_segments(batch_size, data): #batch_size is the segment size
    row=data.shape[0]
    col=data.shape[1]
    #print(row)
    
    if row%batch_size ==0:
        num_batches=int(row/batch_size)
    else:
        num_batches=int(row//batch_size)
        num_batches=num_batches+1
    
    st=np.array([0],dtype=int)
    en=np.array([batch_size-1],dtype=int)
    for i in range(1, num_batches):
        last_en=en[i-1]
        st=np.append(st,last_en+1)
        en =np.append(en,last_en+batch_size)
        
    if(en[-1]>row):
        en[-1]=row
        
    return st,en, num_batches

def random_int():
   return random.randint(1,10)


files = []
for name in glob.glob('/opt/stack/horizon/.blackhole/websockz/chat/ChatApp/trainingData/'+'*.csv'):
  files.append(name)


batch_size=50

features=np.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
#print("After features np array defined in train module") 
#print(features.shape)
for name in files:
    try:
        with open(name) as f:
            #print ("This is name: " + name)
            #print(len(name))
            #print("Before pandas first module") 
            test_data = pd.read_csv(name, header=None) # do what you want
            #print("After pandas first module") 
            #print(test_data.head(10)) 
            
            #print("Before create segemnts first module")
            st,en, num_batches=create_segments(batch_size, test_data)
            #print("after create segments first module") 
            if 'a'==name[65]:
                target=0
            else:
                target=1
            
            
                      
            #print("Test data values only")
            #print (test)
            data = test_data.values   
            for i in range(1, st.shape[0]):
                seg=data[st[i]:en[i],1:]
               # print("seg is this") 
               # print (seg)
                row, col=seg.shape
                #print(col)
                
                ef=np.array([np.percentile(seg[:,0],0),np.percentile(seg[:,0],25),np.percentile(seg[:,0],50),np.percentile(seg[:,0],75),np.percentile(seg[:,0],100),target])
                    
                #feat=np.array([])
                for j in range(1,col):
                    feat=np.array([np.percentile(seg[:,j],0),np.percentile(seg[:,j],25),np.percentile(seg[:,j],50),np.percentile(seg[:,j],75),np.percentile(seg[:,j],100),target])
                    ef=np.hstack((ef,feat))
                
                #print(ef.shape)
                features=np.vstack((features, ef)) 
                
    except IOError as exc:
        if exc.errno != errno.EISDIR:
            raise


# In[7]


features=np.delete(features, 0, 0)

filess = []
for name in glob.glob(testingDataPath):
  filess.append(name)
  print(name)



batch_size=50

Tfeatures=np.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
for name in filess:
    try:
        with open(name) as f:
            test_data = pd.read_csv(name, header=None) # pandas read_csv header=None option for when you data csvs do not have a header or not all data will be read
            #print(test_data.head(10)) 

            st,en, num_batches=create_segments(batch_size, test_data)
            
            if 'a'==name[28]:
                target=0
            else:
                target=1

            test_data_values = test_data.values 
            for i in range(1, st.shape[0]):
                seg=test_data_values[st[i]:en[i],1:]
                row, col=seg.shape
                #print(col)
                
                ef=np.array([np.percentile(seg[:,0],0),np.percentile(seg[:,0],25),np.percentile(seg[:,0],50),np.percentile(seg[:,0],75),np.percentile(seg[:,0],100),target])
                    
                #feat=np.array([])
                for j in range(1,col):
                    feat=np.array([np.percentile(seg[:,j],0),np.percentile(seg[:,j],25),np.percentile(seg[:,j],50),np.percentile(seg[:,j],75),np.percentile(seg[:,j],100),target])
                    ef=np.hstack((ef,feat))
                
                #print(ef.shape)
                Tfeatures=np.vstack((Tfeatures, ef)) 
                
    except IOError as exc:
        if exc.errno != errno.EISDIR:
            raise
            
            
Tfeatures=np.delete(Tfeatures, 0, 0)

trainX=features[:,0:22]
trainY=features[:,23]

testX=Tfeatures[:,0:22]
testY=Tfeatures[:,23]


gnb = GaussianNB()



y_pred = gnb.fit(trainX, trainY).predict(testX)
   
fall = False
for i in y_pred:
   if i == 1:
     fall = True 

if fall == True :
   print("Fall  detected")
else:
   print("No fall detected") 



