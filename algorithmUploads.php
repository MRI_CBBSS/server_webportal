<?php  
/*
*Purpose: This file is responsoble for handling algorithm uploads of Java, Python, or any kind of file that is supported.
*         The first section is devoted to grabing uploaded file information. Then the second section handles file uploading and 
*         processing. 
*
*/

session_start(); 
  


if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']){


           /**************************************************************************************************************************************************
           /Collect the file name, location, size, and other meta data required to upload and process. 
           /*************************************************************************************************************************************************/
           //ensure the $_FILES[] global array is set
           if (isset($_FILES['algorithms'])) { 
             //Grab the necessary information for the file upload process
             $file_name_array = $_FILES['algorithms']['name'];  
             $tmp_name_array = $_FILES['algorithms']['tmp_name'];
             $type_array = $_FILES['algortihms']['type']; 
             $size_array = $_FILES['algorithms']['size']; 
             $error_array = $_FILES['algorithms']['error']; 
             $fileType = "Algorithms";    //may remove. File type for algorithms that is used in the upload_file function to determine directory files will be uploaded to
             $userEmail = $_SESSION['email']; 
            

             /***********************************************************************************************************************************************
             /Checks for essential information. Stops if NA. Information: sensor, package name [if java], purpose
             /***********************************************************************************************************************************************/
             $sensor = $_POST['sensor_name'];
             $purpose = $_POST['purpose_name'];          
             $algorithmName = $_POST['algorithm_name']; 
             
             //Expand the sensor checks to actual sensor types 
             if ($sensor === '') exit('You need to specify a sensor.'); 
             if ($purpose === '') exit('You need to speciy a purpose for the algorithm.'); 
             if ($algorithmName === '') exit('You need to specify an algorithm name.');              

             //will be used for file extension storage and checking
             $file_extension = ""; 
             $file_extension_array = []; 
            

             /**********************************************************************************************************************************************
             /Check for the file extensions. All must be the same or exit. 
             /*********************************************************************************************************************************************/

 
             //ascertain the files' extensions in order to select which file uploads function should be launched
             //additionally if not all file extensions are the same, call an error
             for($i = 0; $i < count($tmp_name_array); $i++) {       
                $file_extension = pathinfo($file_name_array[$i], PATHINFO_EXTENSION); 
                $file_extension_array[$i] = $file_extension; 
             }             

             //this ends the program if more than one file type is present 
             if(count(array_unique($file_extension_array)) != 1) {
               exit( "You can only upload files of one type together."); 
             } 

            //*********************************************************************************************************************************************
            //Begin series of if-statements that will determine if protocols for uploading Java,Python, C++, etc files will be launched based off file type 
            //*********************************************************************************************************************************************

             //operate on java files 
             if($file_extension == 'java') {

               include "upload_file.php";
               $algorithm_name = $_POST['algorithm_name']; 
               $package_name = $_POST['package_name']; 


               $pathToJavaFilesArray = []; 
 
               echo $tmp_name_array[$i] . " " . $file_name_array[$i] . " " . $size_array[$i]; 
               echo "<br>"; 
               try { 
                 $pathToJavaFilesArray =  upload_file($fileType,$tmp_name_array,$file_name_array,$size_array,$userEmail, $package_name, $purpose, $sensor, $algorithm_name);
                    }
               catch (Exception $e) {
                 echo 'Caught exception: ',  $e->getMessage(), "\n"; 
               
               }



             //TODO: Begin compiling the files that were just uploaded if they are compilable.[Java only compilation right now]  
               include "compile_java_code.php";
               if(count($pathToJavaFilesArray) == 1 ) { 
                  $pathToJavaFilesArray[0] = "/opt/stack/horizon/.blackhole/serverFiles/" . $pathToJavaFilesArray[0]; 
                  echo "Here is the path to the uploaded file: " . $pathToJavaFile;  
                  compileJavaDriverFile($pathToJavaFilesArray[0]); 
               } else {
                  $pathToJavaPackage = "/opt/stack/horizon/.blackhole/serverFiles/" . $pathToJavaFilesArray[1]; 
                  $pathToJavaDriverFile = "/opt/stack/horizon/.blackhole/serverFiles/" . $pathToJavaFilesArray[0]; 
                  compileJavaPackage($pathToJavaPackage, $pathToJavaDriverFile);  
               }  //end of java operation code  
            
             /***********************************************************************************************************************
             /Begin handling Python file uploads here
             /***********************************************************************************************************************/
 
             } else if ($file_extension == 'py') {               
               include "upload_python_files.php"; 
           
                try {
                 uploadPythonFiles($fileType,$tmp_name_array,$file_name_array,$size_array,$userEmail, $package_name, $purpose, $sensor);
                }
                catch (Exception $e) {
                 echo 'Caught exception: ',  $e->getMessage(), "\n";
                } 
 
             } else {
                echo "other file. WIll not be uploaded"; 
          } 

      } 
}

 

?>
