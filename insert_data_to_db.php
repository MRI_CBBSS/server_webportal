<?php 
/*
Purpose: This file uploads the dataset the user uploads to the Datasets table in the csufml_db database. 
How it works:  A simple query.  

*/
function uploadDataToDb($folderDir, $file_name_array, $userId, $typeGen)  {
include 'database.php'; 
 
$conn = mysqli_connect(DB_HOST, DB_USER, DB_PSWD, DB_NAME); 


if(!$conn) {
  die("Error: Could not connect to database"); 
} else { 

   $source = 'web'; 
   $streamId = 0; 
   $type = "None"; 
   $humanSubjectType = 'Patient'; 
   $done = 1; 
   $public = 0; 
   $units = "none"; 
   $sensorId = "none"; 
   echo "Inside query"; 
   for($i=0; $i < count($file_name_array); $i++) {
     $file = $file_name_array[$i];  

     $query = "INSERT INTO Datasets (typegen, userId, path, fileName, source, sensorId, type, humanSubjectType, units, done, public) VALUES ('".$typeGen."', '".$userId."', '".$folderDir."','".$file."'
               ,'".$source."'
               ,'".$sensorId."'
               ,'".$type."'
               ,'".$humanSubjectType."'
               ,'".$units."'
               ,'".$done."'
               ,'".$public."'
                )";
 
       
   
    if(mysqli_query($conn, $query)) {
       echo "Records inserted successfully";
    } else {
       echo "ERROR: Could not execute $query. " .mysqli_error($conn);
    } 
     
   }

}
$conn->disconnect(); 
}
?>
