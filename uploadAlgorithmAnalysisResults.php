<?php 
/*
Purpose: Create a text file with the results of a user's analysis. Upload this, and all of the relevant information, into the 
         "Analysis" table in the database.

How it works: There are a lot of additional queries that are used to get all of the information that the Analysis table has been setup to store for each 
              entry into the table. 

*/

session_start(); 

include "databaseFunctions.php"; 


//Algorithm information necessary to acquire the algID value for inserting into the analysis table 
$algName = $_POST['algName']; 
$purpose = $_POST['purpose'];
$extension = $_POST['extension']; 
$algSensorType = $_POST['sensorType']; 

//dataset information necessary to acquire the datasetID for the analysis table
$sensorID = $_POST['sensorID']; 
$streamID = $_POST['streamID']; 
$fileName = $_POST['fileName'];  

//needed to get more unique identifier for correctly ascertaining algID and dataID 
$userEmail = $_SESSION['email']; 


//user analysis results 
$results = $_POST['analResults']; 

$conn = mysqli_connect(DB_HOST,DB_USER, DB_PSWD, DB_NAME);


//step one: get algID and dataID 
 

//TODO: Insert extension into the query to avoid duplicates 
$algIdQuery = "SELECT Distinct algId from Algorithms 
           where algName = '".$algName."' 
           AND   purpose = '".$purpose."'
           AND   signalType = '".$algSensorType."'"; 


$algId = $conn->query($algIdQuery);




if($fileName != "") {
$dataQuery = "SELECT Datasets.dataId from Datasets 
                           WHERE  Datasets.fileName = '".$fileName."'
                           AND    Datasets.userId = '".$userEmail."'
                           AND    Datasets.typeGen = '".$algSensorType."'"; 

} else {
$dataQuery = "select Datasets.dataId from Datasets 
                         WHERE Datasets.streamId='".$streamID."' 
                         AND Datasets.sensorId='".$sensorID."' 
                         AND Datasets.typeGen = '".$algSensorType."'  
                         AND Datasets.userId = '".$userEmail."'";  

} 

$dataId = $conn->query($dataQuery); 


//we want one result. RN not all data is in the query to make ti fully unique
//Change that later but rn do some basic preprocessing before insertion 
while($row = $algId->fetch_assoc()) {
   $algIdValue = $row["algId"]; 
}

while($row = $dataId->fetch_assoc()) { 
  $dataIdValue = $row["dataId"]; 

}




//Now create the result file and store it into the user's section 
$base = getUserPath($userEmail); 
$userAnalysisPath  = $base . '/Analysis-Results'; 
$userAnalysisResultFile = 'Result_1.txt'; 

if (!file_exists($base)) {mkdir($base);} 
if (!file_exists($userAnalysisPath)) {mkdir($userAnalysisPath);}

$wholePath = $userAnalysisPath .'/'. $userAnalysisResultFile; 

$i = 2; 
while(file_exists($wholePath)) {
   $index = strpos($wholePath, "_"); 
   $wholePath = substr_replace($wholePath, strval($i), $index + 1, 1); 
   $i = $i + 1; 
}



$handle = fopen($wholePath, 'w') or die ('Cannot open file: '.$wholePath); 
fwrite($handle, $results); 


//Now insert the information into the database 
$resultFile =  $wholePath; 

$query = "INSERT INTO Analysis (source, dataID, userID, algorithmID, resultFile) 
          VALUES ('web','".$dataIdValue."', '".$userEmail."', '".$algIdValue."', '".$resultFile."')"; 


$result = $conn->query($query); 

$conn->disconnect(); 

?> 
